//
//  RenderService.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 10/9/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage
import Metal
import GLKit
import MetalKit

class RenderService {
    
// MARK: - Child Types
    
    typealias RenderingCompletion = (Result<CGImage>) -> Void
    
    enum RenderingPath {
        case opengl(EAGLContext, GLKView)
        case metal(MTLDevice, MTLCommandQueue, MTKView)
    }
    
// MARK: - Singleton
    
    private static let _sharedInstance = RenderService()
    
    /// Returns the singleton instance of RenderService.
    static var shared: RenderService {
        return _sharedInstance
    }
    
// MARK: - Properties
    
    let renderingPath: RenderingPath
    
    var renderingSurface: UIView {
        switch renderingPath {
        case .opengl(_, let view): return view
        case .metal(_, _, let view): return view
        }
    }
    
    private let ciContext: CIContext
    
    let defaultFromBounds: CGRect
    
    let defaultToBounds: CGRect
    
    private let rgbColorSpace = CGColorSpaceCreateDeviceRGB()
    
// MARK: - Lifecycle
    
    init() {
        let options = [ kCIContextWorkingColorSpace : NSNull() ]
        let renderRect: CGRect
        if
            let metalDevice = MTLCreateSystemDefaultDevice(),
            let queue = metalDevice.makeCommandQueue()
        {
            debugPrint("\\m/_ (o_o) _\\m/ METAL!")
            ciContext = CIContext(mtlDevice: metalDevice, options: options)
            renderRect = UIScreen.main.nativeBounds
            let mView = MTKView(frame: .zero, device: metalDevice)
            mView.translatesAutoresizingMaskIntoConstraints = false
            mView.framebufferOnly = false
            mView.isPaused = true
            mView.contentScaleFactor = 1.0
            renderingPath = .metal(metalDevice, queue, mView)
        } else {
            guard let eaglContext = EAGLContext(api: .openGLES2) else {
                preconditionFailure("Could not create OpenGL shader. We have no way of rendering.")
            }
            ciContext = CIContext(eaglContext: eaglContext, options: options)
            renderRect = UIScreen.main.nativeBounds
            let glView = GLKView(frame: .zero, context: eaglContext)
            glView.translatesAutoresizingMaskIntoConstraints = false
            renderingPath = .opengl(eaglContext, glView)
        }
        defaultFromBounds = renderRect
        defaultToBounds = renderRect
    }
    
// MARK: - Image Rendering
    
    func renderToDisplay(image: CIImage, from: CGRect? = nil, to: CGRect? = nil) {
        let fromRect = from ?? defaultFromBounds
        let toRect = to ?? defaultToBounds
        switch renderingPath {
        case .metal(_, let commandQueue, let mView):
            guard
                let drawable = mView.currentDrawable,
                let buffer = commandQueue.makeCommandBuffer()
            else {
                preconditionFailure("Failed to set up metal rendering.")
            }
            ciContext.render(image, to: drawable.texture, commandBuffer: buffer, bounds: toRect, colorSpace: rgbColorSpace)
            buffer.present(drawable)
            buffer.commit()
            mView.draw()
        case .opengl(_, let view):
            ciContext.draw(image, in: toRect, from: fromRect)
            DispatchQueue.main.async {
                view.setNeedsDisplay()
            }
        }
    }
    
    func renderToBitmap(image: CIImage, from: CGRect? = nil, completion: RenderingCompletion) {
        let renderRect = from ?? image.extent
        if let image = ciContext.createCGImage(image, from: renderRect) {
            completion(.success(image))
        } else {
            completion(.failure)
        }
    }

    func clearDisplay() {
        renderToDisplay(image: CIImage(color: .clear))
    }
}
