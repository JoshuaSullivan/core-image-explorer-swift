//
//  FilterAestheticsManager.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 11/27/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import CoreImage
import UIKit

class FilterAestheticsManager {
    
    private typealias JSONDictionary = [String : Any]
    
    private static let sharedInstance = FilterAestheticsManager()
    
    class func sharedManager() -> FilterAestheticsManager {
        return sharedInstance
    }
    
    private var filterPresets: JSONDictionary = [:]
    
    init() {
        guard let dictionaryURL = Bundle.main.url(forResource: "FilterPresets", withExtension: "json") else {
            assertionFailure("Couldn't find the FilterPresets.json file!")
            return
        }
        guard let data = try? Data(contentsOf: dictionaryURL) else {
            assertionFailure("Couldn't load the filter presets!")
            return
        }
        
        guard let dict = (try? JSONSerialization.jsonObject(with: data, options: [])) as? JSONDictionary else {
            assertionFailure("Couldn't parse the filter presets JSON file.")
            return
        }
        
        filterPresets = dict
    }
    
    func configureFilter(_ filter: CIFilter) {
        for key in filter.inputKeys {
            if let value = valueForFilter(filter.name, inputKey: key) {
                filter.setValue(value, forKey: key)
            }
        }
    }
    
    private func valueForFilter(_ filterIdentifier: String, inputKey: String) -> Any? {
        guard let filterDict = filterPresets[filterIdentifier] as? JSONDictionary else {
            return aestheticDefaultForKey(inputKey)
        }
        return filterDict[inputKey] ?? aestheticDefaultForKey(inputKey)
    }
    
    private func aestheticDefaultForKey(_ inputKey: String) -> Any? {
        let screenBounds = UIScreen.main.nativeBounds
        switch inputKey {
        case kCIInputCenterKey:
            return CIVector(x: screenBounds.width / 2.0, y: screenBounds.height / 2.0)
        case "inputMessage":
            return "Core Image Filter Explorer".data(using: String.Encoding.utf8) as AnyObject?
        case "inputCubeDimension":
            return 64 as AnyObject?
        case "inputCubeData":
            guard let img = UIImage(named: "NightVisionColorCube") else {
                return nil
            }
            guard let data = try? ColorCubeHelper.createColorCubeData(inputImage: img, cubeDimension: 64) else {
                return nil
            }
            return data
        case "inputExtent":
            return CIVector(cgRect: UIScreen.main.nativeBounds)
        default: return nil
        }
    }
    
    func shouldNormalizeOutputForFilter(_ filterName: String) -> Bool {
        switch filterName {
        case "CILenticularHaloGenerator", "CISunbeamsGenerator" :
            return false
        default: return true
        }
    }
}
