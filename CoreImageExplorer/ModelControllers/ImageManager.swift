//
//  ImageManager.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 7/19/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage

enum ImageType: String {
    case image = "Image"
    case grayscaleMask = "GrayscaleMask"
    case alphaMask = "AlphaMask"
    case video = "Video"
}

enum ImageOrientation: String {
    case landscape = "Landscape"
    case portrait = "Portrait"
}

enum ImageIntent: String {
    case thumbnail = "Thumbnail"
    case rendering = "Rendering"
}

class ImageManager {
    
    typealias ImageRequestCompletion = (_ image: CIImage?) -> Void
    typealias ThumbnailRequestCompletion = (_ image: UIImage?) -> Void
    
    static let sharedManager = ImageManager()
    
    private let ciContext: CIContext
    
    private let thumbnailScale: CGFloat = 1.0 / 3.0
    
    private lazy var samplesBaseURL: URL = {
        if let cachesFolder = FileManager.default.urls(for: .cachesDirectory, in: .userDomainMask).first {
            return cachesFolder
        } else if let docsFolder = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first {
            print("Warning: Falling back to Documents directory.")
            return docsFolder
        } else {
            preconditionFailure("Couldn't get a storage folder!")
        }
    }()
    
    private var currentOrientation: ImageOrientation {
        switch UIDevice.current.orientation {
        case .landscapeLeft, .landscapeRight:
            return .landscape
        default:
            return .portrait
        }
    }
    
    /// The bounds to use for thumbnails in portrait orientation.
    private var thumbBounds: CGRect {
        let dim: CGFloat = 120.0 * UIScreen.main.nativeScale
        return CGRect(x: 0, y: 0, width: dim, height: dim)
    }
    /// The bounds to use for full-size images in portrait orientation.
    private let renderBoundsPortrait: CGRect
    /// The bounds to use for full-size images in landscape orientation.
    private let renderBoundsLandscape: CGRect
    
    private var sources: [ImageType : [ImageSource]] = [:]
    
    init() {
        
        let eaglContext = EAGLContext(api: .openGLES2)
        ciContext = CIContext(eaglContext: eaglContext!)
        
        //        if let metalDevice = MTLCreateSystemDefaultDevice() {
        //            print("Using Metal-based context.")
        //            ciContext = CIContext(MTLDevice: metalDevice)
        //        } else {
        //            print("Using OpenGL-based context.")
        //            let eaglContext = EAGLContext(API: .OpenGLES2)
        //            ciContext = CIContext(EAGLContext: eaglContext)
        //        }
        renderBoundsPortrait = UIScreen.main.nativeBounds
        renderBoundsLandscape = CGRect(x: 0.0, y: 0.0, width: renderBoundsPortrait.height, height: renderBoundsPortrait.width)
        
        guard let imageDataURL = Bundle.main.url(forResource: "SampleImageManifest", withExtension: "plist") else {
            assertionFailure("Can't find image source data file.")
            return
        }
        guard let rawDict = NSDictionary(contentsOf: imageDataURL) else {
            assertionFailure("Unable to load image source data file.")
            return
        }
        
        let imageTypes: [ImageType] = [.image, .grayscaleMask, .alphaMask]
        for type in imageTypes {
            let key = dataKeyForImageType(type)
            guard let dataArr = rawDict[key] as? [AttributeDictionary] else {
                assertionFailure("Couldn't find '\(key)' entry in data dictionary.")
                continue
            }
            self.sources[type] = dataArr.compactMap { ImageSource(data: $0) }
        }
    }
    
    func createSampleImagesIfNeeded() {
        DispatchQueue.global(qos: .default).async {
            let types: [ImageType] = [.image, .grayscaleMask, .alphaMask]
            let orientations: [ImageOrientation] = [.landscape, .portrait]
            let intents: [ImageIntent] = [.thumbnail, .rendering]
            for type in types {
                guard let sourceArr = self.sources[type] else {
                    assertionFailure("Couldn't load sources for \(type).")
                    continue
                }
                let sourceCount = sourceArr.count
                for orientation in orientations {
                    for intent in intents {
                        for index in 0..<sourceCount {
                            if !self.imageSourceExists(type, index: index, orientation: orientation, intent: intent) {
                                if let sampleImage = self.createSource(type, index: index, orientation: orientation, intent: intent) {
                                    // Save the image.
                                    self.saveSample(sampleImage, type: type, index: index, orientation: orientation, intent: intent)
                                } else {
                                    assertionFailure("Failed to create sample image.")
                                }
                            } else {
                                print("Found existing sample image for:", type, orientation, intent)
                            }
                        }
                    }
                }
            }
        }
    }
    
    private func saveSample(_ image: UIImage, type: ImageType, index: Int, orientation: ImageOrientation, intent: ImageIntent) {
        DispatchQueue.global(qos: .utility).async {
            guard let imageData = UIImagePNGRepresentation(image) else {
                assertionFailure("Failed to encode image to PNG.")
                return
            }
            let imageURL = self.urlForSource(type, index: index, orientation: orientation, intent: intent)
            if (try? imageData.write(to: imageURL, options: [])) != nil {
                print("Successfully wrote image to:", imageURL)
                return
            } else {
                print("Failed to write image to:", imageURL)
                return
            }
        }
    }
    
    func getImage(_ type: ImageType, index: Int, orientation:ImageOrientation, intent:ImageIntent, completion:@escaping ImageRequestCompletion) {
        let url = urlForSource(type, index: index, orientation: orientation, intent: intent)
        if (url as NSURL).checkPromisedItemIsReachableAndReturnError(nil),
            let image = CIImage(contentsOf: url, options: [kCIImageColorSpace:NSNull()]) {
            completion(image)
            return
        } else {
            print("Failed to get image from disk. Recreating it.")
            DispatchQueue.global(qos: .default).async {
                if let image = self.createSource(type, index: index, orientation: orientation, intent: intent) {
                    if let imageData = UIImagePNGRepresentation(image) {
                        try? imageData.write(to: url, options: [])
                    }
                    DispatchQueue.main.async {
                        completion(CIImage(image: image, options: [kCIImageColorSpace:NSNull()])!)
                    }
                } else {
                    DispatchQueue.main.async {
                        completion(nil)
                    }
                }
            }
        }
    }
    
    func getRenderingImageForCurrentOrientation(_ type: ImageType, index: Int, completion: @escaping ImageRequestCompletion) {
        getImage(type, index: index, orientation: currentOrientation, intent: .rendering, completion: completion)
    }
    
    func getThumbnailImage(_ type: ImageType, index: Int, completion: @escaping ThumbnailRequestCompletion) {
        if imageSourceExists(type, index: index, orientation: currentOrientation, intent: .thumbnail) {
            let imgURL = urlForSource(type, index: index, orientation: currentOrientation, intent: .thumbnail)
            DispatchQueue.global(qos: .default).async {
                let imagePath = imgURL.path
                if let image = UIImage(contentsOfFile: imagePath)
                {
                    DispatchQueue.main.async {
                        completion(image)
                    }
                } else {
                    DispatchQueue.main.async {
                        debugPrint("Unable to load thumbnail.")
                        completion(nil)
                    }
                }
            }
        } else {
            if let thumb = createSource(type, index: index, orientation: .landscape, intent: .thumbnail) {
                saveSample(thumb, type: type, index: index, orientation: .landscape, intent: .thumbnail)
                completion(thumb)
            } else {
                completion(nil)
            }
        }
    }
    
    func getImageTypeCount(_ type: ImageType) -> Int {
        guard let sourceArr = sources[type] else {
            assertionFailure("Couldn't get sources for \(type).")
            return 0
        }
        return sourceArr.count
    }
    
    func displayNameForImageType(_ type: ImageType, index: Int) -> String? {
        guard let sourceArray = sources[type] else {
            assertionFailure("Can't find sources for type: \(type)")
            return nil
        }
        guard index < sourceArray.count else {
            assertionFailure("index is out of range!")
            return nil
        }
        let source = sourceArray[index]
        return source.name
    }
    
    private func createSource(_ type: ImageType, index: Int, orientation:ImageOrientation, intent:ImageIntent) -> UIImage? {
        print("Creating source image with parameters:", type, orientation, intent)
        guard type != .video else {
            assertionFailure("Cannot create a source image for video.")
            return nil
        }
        guard let sourceArr = sources[type] else {
            assertionFailure("Couldn't find the image sources for \(type).")
            return nil
        }
        guard index < sourceArr.count else {
            assertionFailure("index out of bounds.")
            return nil
        }
        let imageData = sourceArr[index]
        let rawFileNameComponents = imageData.source.components(separatedBy: ".")
        guard let sourceURL = Bundle.main.url(forResource: rawFileNameComponents[0], withExtension: rawFileNameComponents[1]) else {
            assertionFailure("ERROR: Can't find file '\(type.rawValue)'.")
            return nil
        }
        guard let rawSource = CIImage(contentsOf: sourceURL) else {
            assertionFailure("ERROR: Can't create CIImage with URL: \(sourceURL)")
            return nil
        }
        
        // Determine the target bounds for the image.
        let targetBounds: CGRect
        switch intent {
        case .rendering:
            targetBounds = orientation == .portrait ? renderBoundsPortrait : renderBoundsLandscape
        case .thumbnail:
            targetBounds = thumbBounds
        }
        
        // Calculate the amount of down-scaling needed.
        let imageBounds = rawSource.extent
        let dw = targetBounds.width / imageBounds.width
        let dh = targetBounds.height / imageBounds.height
        let scale = max(dw, dh) * 1.02 // Leaving a bit of overflow
        
        // Scale the source image down
        let scaleTransform = CGAffineTransform(scaleX: scale, y: scale)
        let scaledImage = rawSource.transformed(by: scaleTransform)
        let scaledBounds = scaledImage.extent
        
        // Center the scaled image
        let xOffset = (targetBounds.width - scaledBounds.width) / 2.0
        let yOffset = (targetBounds.height - scaledBounds.height) / 2.0
        let offsetTransform = CGAffineTransform(translationX: xOffset, y: yOffset)
        let scaledOffsetImage = scaledImage.transformed(by: offsetTransform)
        
        // Render the final image and convert it to a UIImage
        let renderedImage = ciContext.createCGImage(scaledOffsetImage, from: targetBounds)
        let screenScale = UIScreen.main.scale
        let finalImage = UIImage(cgImage: renderedImage!, scale: screenScale, orientation: .up)
        return finalImage
    }
    
    
    
    private func imageSourceExists(_ type: ImageType, index: Int, orientation:ImageOrientation, intent:ImageIntent) -> Bool {
        return (urlForSource(type, index: index, orientation: orientation, intent: intent) as NSURL).checkResourceIsReachableAndReturnError(nil);
    }
    
    private func urlForSource(_ type: ImageType, index: Int, orientation:ImageOrientation, intent:ImageIntent) -> URL {
        return samplesBaseURL.appendingPathComponent(nameForSource(type, index: index, orientation: orientation, intent: intent))
    }
    
    private func nameForSource(_ type: ImageType, index: Int, orientation:ImageOrientation, intent:ImageIntent) -> String {
        let deviceScale = UIScreen.main.nativeScale
        let scaleString = deviceScale > 1.0 ? "@\(Int(deviceScale))x" : ""
        if intent == .rendering {
            return "\(type.rawValue)\(index)-\(orientation.rawValue)-\(intent.rawValue)\(scaleString).png"
        } else {
            return "\(type.rawValue)\(index)-\(intent.rawValue)\(scaleString).png"
        }
    }
    
    private func dataKeyForImageType(_ type: ImageType) -> String {
        switch type {
        case .image: return "image"
        case .grayscaleMask: return "grayscaleMask"
        case .alphaMask: return "alphaMask"
        default:
            assertionFailure("Not implemented yet.")
            return "image"
        }
    }
}
