//
//  VideoProvider.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 4/3/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit
import AVFoundation

protocol VideoProviderObserver: class {
    func videoProvider(_ videoProvider: VideoProvider, gotFrame ciImage: CIImage)
}

class VideoProvider: NSObject {
    
    /// Since you can't have arrays of weak references, we'll make an array of objects holding
    /// weak references to the other objects.
    private struct Observer {
        weak var observer: VideoProviderObserver?
        
        init(_ observer: VideoProviderObserver) {
            self.observer = observer
        }
    }
    
    // MARK: - Singleton
    private static var _shared: VideoProvider = VideoProvider()
    
    /// Singleton instance.
    class var shared: VideoProvider {
        return _shared
    }
    
    // MARK: - Properties
    
    private var captureSession: AVCaptureSession
    
    private var observers: [Observer] = []
    
    private let dispatchQueue: DispatchQueue
    
    private var isRunning: Bool = false
    
    private let inputs: [AVCaptureDeviceInput]
    
    private var currentInput: AVCaptureDeviceInput?
    
    private var inputIndex: Int = 0    
    
    // Mark Lifecycle
    override init() {
        // Create the capture session.
        captureSession = AVCaptureSession()

        captureSession.beginConfiguration()
        
        // Create the dispatch queue that the camera works on.
        dispatchQueue = DispatchQueue(label: "com.hairlessape.cife.video-buffer")
        
        // Set the capture quality.
        captureSession.sessionPreset = .high
        
        // Discover the available input devices and make AVCaptureDeviceInputs with them.
        var devices: [AVCaptureDevice] = []
        // Find all ze cameras!
        let deviceTypes: [AVCaptureDevice.DeviceType] = [.builtInTelephotoCamera, .builtInWideAngleCamera]
        let discoverySession = AVCaptureDevice.DiscoverySession(deviceTypes: deviceTypes, mediaType: AVMediaType.video, position: .unspecified)
        devices = discoverySession.devices
        
        // Map any discovered devices into AVCaptureDeviceInputs.
        inputs = devices.compactMap({ try? AVCaptureDeviceInput(device: $0) })
        
        // Call super now that we've stored all properties so we can start using self.
        super.init()

        NotificationCenter.default.addObserver(forName: .AVCaptureSessionRuntimeError, object: nil, queue: nil) { note in
            guard let error = note.userInfo?[AVCaptureSessionErrorKey] as? Error else {
                print("VideoProvide: AVCaptureSession failed to start for an unknown reason.")
                return
            }
            print("SESSION FAILED TO START: \(error.localizedDescription)")
        }
        
        // If we found no inputs, we're done.
        guard !inputs.isEmpty else { return }
        

        
        let videoOutput = AVCaptureVideoDataOutput()
        videoOutput.setSampleBufferDelegate(self, queue: dispatchQueue)
        if captureSession.canAddOutput(videoOutput) {
            captureSession.addOutput(videoOutput)
        } else {
            print("Unable to add output: \(videoOutput)")
        }

        // Select a back-facing camera by default.
        if
            let backCamera = inputs.filter({ $0.device.position == .back }).first,
            let index = inputs.index(of: backCamera)
        {
            setInput(index: index)
        }
        captureSession.commitConfiguration()
    }
    
    private func setInput(index: Int) {
        guard !inputs.isEmpty else { return }
        guard index < inputs.count else {
            assertionFailure("index was out of range.")
            return
        }
        inputIndex = index
        let nextInput = inputs[index]
        let wasRunning = isRunning
        stopCapture()
        captureSession.beginConfiguration()
        if let currentInput = currentInput {
            captureSession.removeInput(currentInput)
        }
        if captureSession.canAddInput(nextInput) {
            captureSession.addInput(nextInput)
        } else {
            print("Failed to add input: \(nextInput)")
        }
        correctVideoOrientation()
        captureSession.commitConfiguration()
        currentInput = nextInput
        if wasRunning {
            startCapture()
        }
    }

    private func correctVideoOrientation() {
        let screenOrientation = UIDevice.current.orientation.rawValue
        guard let orientation = AVCaptureVideoOrientation(rawValue: screenOrientation) else { return }

        captureSession.outputs
            .flatMap { $0.connections }
            .filter { $0.inputPorts.contains(where: { $0.mediaType == .video }) }
            .forEach { $0.videoOrientation = orientation }
    }
    
    /// Add a video observer which will receive new video frames as they become available.
    func add(observer: VideoProviderObserver) {
        observers.append(Observer(observer))
        startCapture()
    }
    
    /// Remove a previously-registered video observer.
    func remove(observer: VideoProviderObserver) {
        observers = observers.filter({ $0.observer !== observer })
        if observers.isEmpty {
            stopCapture()
        }
    }
    
    var cameraCount: Int {
        return inputs.count
    }
    
    func switchToNextCamera() {
        guard !inputs.isEmpty else {
            debugPrint("There are no inputs available, so calling switchToNextCamera does nothing.")
            return
        }
        let index = (inputIndex + 1) % inputs.count
        setInput(index: index)
    }
    
    private func startCapture() {
        guard !isRunning else { return }
        isRunning = true
        captureSession.startRunning()
    }
    
    private func stopCapture() {
        guard isRunning else { return }
        isRunning = false
        captureSession.stopRunning()
    }
}

extension VideoProvider: AVCaptureVideoDataOutputSampleBufferDelegate {
    func captureOutput(_ output: AVCaptureOutput,
                       didOutput sampleBuffer: CMSampleBuffer,
                       from connection: AVCaptureConnection)
    {
        guard let image = CIImage(sampleBuffer: sampleBuffer) else {
            return
        }
        let imageSize = image.extent.size

        let targetSize = UIScreen.main.nativeBounds.size
        let scale = max(targetSize.width / imageSize.width, targetSize.height / imageSize.height)
        let scaledImage = image.transformed(by: CGAffineTransform(scaleX: scale, y: scale))
        let scaledSize = scaledImage.extent.size
        let dx = -round((scaledSize.width - targetSize.width) / 2.0)
        let dy = -round((scaledSize.height - targetSize.height) / 2.0)
        let translatedImage = scaledImage.transformed(by: CGAffineTransform(translationX: dx, y: dy))
        let finalImage = translatedImage.cropped(to: UIScreen.main.nativeBounds)

        // Prune any observers that have become nil.
        observers = observers.filter({ $0.observer != nil })

        // If pruning caused all observers to disappear, then stop running.
        guard !observers.isEmpty else {
            stopCapture()
            return
        }

        // Send video frame to observers on the main thread.
        DispatchQueue.main.async {
            self.observers.forEach({ $0.observer?.videoProvider(self, gotFrame: finalImage) })
        }
    }
}
