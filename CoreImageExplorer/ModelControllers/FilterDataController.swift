//
//  FilterDataController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 7/16/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import Foundation
import CoreImage

class FilterDataController {
    
    /// Singleton instance
    static let sharedController = FilterDataController()
    
    /// FilterDescriptor objects for all available CIFilters.
    let filterDescriptors : [FilterDescriptor]
    
    /// A lookup by category for FilterDescriptor objects.
    let filterCategoryMap : [String : [FilterDescriptor]]
    
    init() {
        let filterIdentifiers = CIFilter.filterNames(inCategory: nil)
        filterDescriptors = filterIdentifiers.flatMap { FilterDescriptor($0) }
        var catMap : [String : [FilterDescriptor]] = filterDescriptors.reduce([:]) { (dict, filter) in
            var newDict = dict
            for category in filter.categories {
                if newDict[category] == nil {
                    newDict[category] = [FilterDescriptor]()
                }
                newDict[category]?.append(filter)
            }
            return newDict
        }
        catMap[kCICategoryBuiltIn] = nil
        filterCategoryMap = catMap
        
        generateFilterDescriptors()
    }
    
    func generateFilterDescriptors() {
        
        let filterIdentifiers = CIFilter.filterNames(inCategory: kCICategoryBuiltIn)
        let descriptors = filterIdentifiers.flatMap { FilterDescriptor($0) }
        var categoryMap: [String: [FilterDescriptor]] = [:]
        var categoryNames = Set<String>()
        descriptors.forEach {
            descriptor in
            descriptor.categories.forEach {
                category in
                categoryNames.insert(category)
                if categoryMap[category] == nil {
                    categoryMap[category] = []
                }
                categoryMap[category]!.append(descriptor)
            }
        }
    }
}
