//
//  VideoInputManager.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 4/3/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit
import AVFoundation

protocol VideoInputDelegate: class {
    func videoInputManager(_ videoInput: VideoInputManager, didUpdateForInput input: String, ofFilter filter: CIFilter)
}

open class VideoInputManager: NSObject {

    // MARK: - Singleton
    fileprivate static var _sharedManager: VideoInputManager = VideoInputManager()

    /// Singleton instance.
    open class func sharedManager() -> VideoInputManager {
        return _sharedManager
    }

    // MARK: - Properties

    /// The delegate will receive a call when the video frame updates.
    weak var delegate: VideoInputDelegate?

    /// The filter we are providing video for.
    fileprivate weak var filter: CIFilter?

    /// The property to assign the video frames to.
    fileprivate var input: String = ""

    fileprivate var captureSession: AVCaptureSession

    // Mark Lifecycle
    override init() {
        captureSession = AVCaptureSession()
        super.init()
    }
    
    /// Start video capture for the provided input on the provided filter.
    ///
    /// - parameter filter: The filter to apply video to.
    /// - parameter input: The name of the input to target.
    open func startVideoForFilter(_ filter: CIFilter, input: String) {
        self.filter = filter
        self.input = input
    }

    /// Stop capturing video.
    open func stopVideoCapture() {
        self.filter = nil
    }

}

extension VideoInputManager: AVCaptureVideoDataOutputSampleBufferDelegate {
    public func captureOutput(_ captureOutput: AVCaptureOutput,
                       didOutput didOutputSampleBuffer: CMSampleBuffer,
                       from fromConnection: AVCaptureConnection) {

    }
}
