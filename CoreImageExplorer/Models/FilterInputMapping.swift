//
//  FilterInputMapping.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 10/18/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import CoreImage

struct FilterInputMapping {
    let descriptor: FilterDescriptor
    
    var inputMap: [String : CIImage] = [:]
    
    init(descriptor: FilterDescriptor) {
        self.descriptor = descriptor
    }
    
    func applyInputMapToFilter(_ filter: CIFilter) {
        filter.inputKeys.forEach { key in filter.setValue(inputMap[key], forKey: key) }
    }
}
