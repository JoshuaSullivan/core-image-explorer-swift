//
//  ImageSource.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/20/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import Foundation

struct ImageSource {
    struct Keys {
        static let id = "id"
        static let name = "displayName"
        static let source = "source"
    }
    
    let id: String
    let name: String
    let source: String
    
    init?(data: AttributeDictionary) {
        guard let
            id = data[Keys.id] as? String,
            let name = data[Keys.name] as? String,
            let source = data[Keys.source] as? String
            else {
                debugPrint("Couldn't find all required properties in the data dictionary.")
                return nil
        }
        self.id = id
        self.name = name
        self.source = source
    }
}

extension ImageSource: Equatable {}

func == (lhs: ImageSource, rhs: ImageSource) -> Bool {
    return lhs.id == rhs.id
}
