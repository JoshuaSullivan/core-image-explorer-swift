//
//  FilterDescriptor.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 10/18/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import Foundation
import CoreImage

struct FilterDescriptor {
    /// The identifier for the filter.
    let filterId : String
    
    /// The display name of the filter.
    let displayName : String
    
    /// The categories the filter belongs to.
    let categories : [String]
    
    init?(_ filterId : String) {
        guard let filter = CIFilter(name: filterId) else {
            return nil
        }
        // From here down we're doing forced unwrapping because these attributes will always be available if the filter exists.
        displayName = CIFilter.localizedName(forFilterName: filterId)!
        categories = filter.attributes[kCIAttributeFilterCategories]! as! [String]
        self.filterId = filterId
    }
}

extension FilterDescriptor: Equatable {}
func ==(lhs: FilterDescriptor, rhs: FilterDescriptor) -> Bool {
    return lhs.filterId == rhs.filterId
}

extension FilterDescriptor: CustomStringConvertible {
    var description: String {
        return filterId
    }
}
