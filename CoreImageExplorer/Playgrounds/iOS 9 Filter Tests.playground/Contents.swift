//: Playground - noun: a place where people can play

import UIKit
import CoreImage

let allFilterIdentifiers = CIFilter.filterNamesInCategory(kCICategoryBuiltIn)
let filters = allFilterIdentifiers.flatMap{ CIFilter(name: $0) }
//var inputSet = Set<String>()
filters.forEach {
    filter in
    filter.inputKeys.forEach {
        key in
        if let inputAttributes = filter.attributes[key] {
            let inputClass = inputAttributes[kCIAttributeClass] as! String
            if inputClass == "CIVector" {
                print(filter.name, "/", key, ":", inputAttributes)
            }
        }
    }
}
//let sortedInputs = inputSet.sort(<)
//print(sortedInputs.joinWithSeparator("\n"))
