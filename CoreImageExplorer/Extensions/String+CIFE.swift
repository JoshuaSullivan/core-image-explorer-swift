//
//  String+CIFE.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/13/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import Foundation

extension String {
    func stringBySplittingCamelCase() -> String {
        let charSet = CharacterSet.uppercaseLetters.union(CharacterSet.decimalDigits)
        var outString = ""
        var isFirst = true
        self.unicodeScalars.forEach {
            scalar in
            if charSet.contains(scalar) && !isFirst {
                outString += " "
            }
            outString += String(describing: scalar)
            isFirst = false
        }
        return outString
    }
    
    func range(from nsRange: NSRange) -> Range<String.Index>? {
        guard
            let from16 = utf16.index(utf16.startIndex, offsetBy: nsRange.location, limitedBy: utf16.endIndex),
            let to16 = utf16.index(from16, offsetBy: nsRange.length, limitedBy: utf16.endIndex),
            let from = String.Index(from16, within: self),
            let to = String.Index(to16, within: self)
            else { return nil }
        return from ..< to
    }
    
    
}
