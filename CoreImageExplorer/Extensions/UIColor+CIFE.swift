//
//  UIColor+CIFE.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 11/29/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

extension UIColor {
    
    /// Create a UIColor from the supplied hex value with the assumed format 0xRRGGBB.
    convenience init(hex:UInt) {
        self.init(hex: hex, alpha: 1.0)
    }
    
    /// Create a UIColor from the supplied hex value with the assumed format 0xRRGGBB and an alpha value.
    convenience init(hex: UInt, alpha: CGFloat) {
        let r = CGFloat((hex & 0xFF0000) >> 16) / 255.0
        let g = CGFloat((hex & 0x00FF00) >> 8) / 255.0
        let b = CGFloat(hex & 0x0000FF) / 255.0
        self.init(red: r, green: g, blue: b, alpha: alpha)
    }
    
    class func paleYellowColor() -> UIColor {
        return UIColor(hex: 0xFFFFA2)
    }
}
