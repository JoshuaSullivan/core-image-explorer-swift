//
//  CIImage+CIFE.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 11/13/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import AVFoundation
import CoreImage

extension CIImage {
    convenience init?(sampleBuffer: CMSampleBuffer) {
        guard let imageBuffer = CMSampleBufferGetImageBuffer(sampleBuffer) else {
            return nil
        }
        self.init(cvImageBuffer: imageBuffer)
    }
}
