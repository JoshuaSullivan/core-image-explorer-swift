//
//  UIView+CIFE.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 9/5/18.
//  Copyright © 2018 Joshua Sullivan. All rights reserved.
//

import UIKit

extension UIView {
    @discardableResult public func parent(otherView: UIView, with insets: UIEdgeInsets = .zero) -> [NSLayoutConstraint] {
        self.addSubview(otherView)
        let constraints = [
            otherView.topAnchor.constraint(equalTo: self.topAnchor, constant: insets.top),
            otherView.leadingAnchor.constraint(equalTo: self.leadingAnchor, constant: insets.right),
            otherView.bottomAnchor.constraint(equalTo: self.bottomAnchor, constant: insets.bottom),
            otherView.trailingAnchor.constraint(equalTo: self.trailingAnchor, constant: insets.left)
        ]
        constraints.forEach({ $0.isActive = true })
        return constraints
    }
}
