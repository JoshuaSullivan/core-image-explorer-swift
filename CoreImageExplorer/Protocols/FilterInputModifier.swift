//
//  FilterInputModifier.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/27/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import Swift

/// Objects conforming to FilterInputModifier can be used to set the attributes of CIFilters.
protocol FilterInputModifier {
    
    /// The name of the input to modify.
    var inputName: String! { get set }
    
    /// The starting attribute dictionary for the property.
    var inputAttributes: AttributeDictionary! { get set }
    
    /// The starting value for the input.
    var initialValue: Any! { get set }
    
    /// The delegate will be informed when the FilterInputModifier affects change to its specified input parameter.
    weak var delegate: FilterInputModifierDelegate? { get set }
}

/// Objects conforming to FilterInputModifierDelegate can be informed when a FilterInputModifier changes its value.
protocol FilterInputModifierDelegate: class {
    
    /// Notify the delegate that the specific input was set to the specified value.
    func filterInputModifier(_ modifier: FilterInputModifier, didSetInput input: String, toValue value: Any)
    
    func filterInputModifier(_ modifier: FilterInputModifier, didRequestVideoForInput input: String)

    func filterInputModifier(_ modifier: FilterInputModifier, didStopVideoForInput input: String)

    func filterInputModifierChangeCamera(_ modifier: FilterInputModifier)
}

extension FilterInputModifierDelegate {
    func filterInputModifierChangeCamera(_ modifier: FilterInputModifier) {
        VideoProvider.shared.switchToNextCamera()
    }
}
