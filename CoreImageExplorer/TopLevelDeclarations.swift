//
//  TopLevelDeclarations.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/27/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

typealias AttributeDictionary = [String : Any]

public enum Result<T> {
    case success(T)
    case failure
}
