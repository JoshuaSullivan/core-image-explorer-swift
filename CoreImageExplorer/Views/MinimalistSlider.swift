//
//  MinimalistSlider.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 10/19/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

open class MinimalistSlider: UIControl {
    
    public enum Orientation {
        case `default`, horizontal, vertical
    }
    
    /** The value of the slider control. */
    open var value: CGFloat = 0.0 {
        didSet {
            let valueString : String
            if (integralValues) {
                valueString = String(NSString(format: "%.0f", value))
            } else {
                valueString = String(NSString(format: "%0.\(numericPrecision)f" as NSString, value))
            }
            self.showValueLabel(valueString)
        }
    }
    
    /** The axis of interaction for the slider. When Default, the control will automatically select the longer
     of the two axes for interaction. Otherwise, it will use the specified axis. */
    open var orientation: Orientation = .default
    
    /** The minimum value of the slider. */
    @IBInspectable open var minimumValue: CGFloat = 0.0 {
        didSet {
            if value < minimumValue {
                value = minimumValue
            }
            calculatePrecision()
        }
    }
    
    /** The maximum value of the slider. */
    @IBInspectable open var maximumValue: CGFloat = 1.0 {
        didSet {
            if value > maximumValue {
                value = maximumValue
            }
            calculatePrecision()
        }
    }
    
    /** When set to true, the control will return integer values, rather than floating-point ones. */
    @IBInspectable open var integralValues : Bool = false
    
    /** When set to true (default), the slider will display the current value as it changes. */
    @IBInspectable open var showsValueLabel : Bool = true
    
    /** When set to true (default), the slider will hide the value label after a short period of inactivity. */
    @IBInspectable open var autoHideValueLabel : Bool = true
    
    @IBInspectable open var title: String? {
        didSet {
            titleLabel?.text = title
        }
    }
    
    /// The color of the backdrop (alpha is used) placed behind the text labels. This will only be created if the
    /// user does not specify their own title and value labels.
    @IBInspectable open var backdropColor: UIColor = UIColor(white: 0.0, alpha: 0.75) {
        didSet {
            labelBackdrop?.backgroundColor = backdropColor
        }
    }
    
    /** The number of seconds after the value changes stop to trigger the fade-out animation on the label. */
    open var fadeOutTime : TimeInterval = 0.75
    
    /** Controls the "dead zone" at each end of the slider, which makes it possible to achieve the maximum
     and minimum values when the slider extends to the edge of the screen. */
    open var touchInsetPercent : CGFloat = 0.025
    
    /** Controls the fade-out time of the control. */
    private var autoHideTimer : Timer?
    
    /** The label used to display the slider value. If it does not exist when it is first called upon to appear, it will be created. */
    @IBOutlet open weak var valueLabel : UILabel?
    
    /** The label that displays an arbitrary text label above the value. */
    @IBOutlet open weak var titleLabel: UILabel? {
        didSet {
            guard let tl = titleLabel else { return }
            tl.textColor = tintColor
        }
    }
    
    open weak var labelBackdrop: UIView?
    
    /** The view used as the positional indicator. */
    private var indicator : UIView? = nil
    
    /** Keeps track of whether the view is in a vertical or horizontal configuration. */
    private var isHorizontal = true
    
    /** The precision of numbers sent to the label. */
    private var numericPrecision : Int = 3
    
    //MARK: - View Lifecycle
    
    override open func awakeFromNib() {
        super.awakeFromNib()
        if value < minimumValue {
            value = minimumValue
        } else if value > maximumValue {
            value = maximumValue
        }
    }
    
    override open func layoutSubviews() {
        super.layoutSubviews()
        
        //TODO: Track whether or not the labels were created by user or class setup and change layout parameters accordingly.
        if let backdropFrame = calculateLabelBackdropFrame() {
            labelBackdrop?.frame = backdropFrame
        } else {
            labelBackdrop?.isHidden = true
        }
        
    }
    
    //MARK: - Label Methods
    
    /** Show the value, when enabled. */
    open func showValueLabel(_ value:String) {
        if !showsValueLabel { return }
        
        if valueLabel == nil {
            createAndLayoutLabels()
        }
        
        if let ttl = title , !ttl.isEmpty {
            titleLabel?.isHidden = false
            titleLabel?.text = ttl
        } else {
            titleLabel?.isHidden = true
        }
        
        guard let lbl = valueLabel else {
            assertionFailure("Why does the label not exist at this point?")
            return
        }
        
        if let backdropFrame = calculateLabelBackdropFrame() {
            self.labelBackdrop?.frame = backdropFrame
            self.labelBackdrop?.isHidden = false
        } else {
            self.labelBackdrop?.isHidden = true
        }
        
        lbl.isHidden = false
        titleLabel?.isHidden = false
        lbl.text = value
        lbl.textColor = self.tintColor
        UIView.animate(withDuration: 0.15,
            animations: {
                lbl.alpha = 1.0
                self.titleLabel?.alpha = 1.0
                self.labelBackdrop?.alpha = 1.0
            },
            completion: {
                completed in
                if self.autoHideValueLabel {
                    self.startLabelTimer()
                }
        })
    }
    
    open func hideValueLabel() {
        guard let lbl = valueLabel else {
            return
        }
        if lbl.isHidden { return }
        UIView.animate(withDuration: 0.4,
            animations: {
                lbl.alpha = 0.0
                self.titleLabel?.alpha = 0.0
                self.labelBackdrop?.alpha = 0.0
            },
            completion: {
                completed in
                lbl.isHidden = true
                self.titleLabel?.isHidden = true
                self.labelBackdrop?.isHidden = true
        })
    }
    
    private func createAndLayoutLabels() {
        if titleLabel != nil && valueLabel != nil { return }
        
        let labelBackdrop = UIView(frame: CGRect.zero)
        labelBackdrop.backgroundColor = backdropColor
        labelBackdrop.layer.cornerRadius = 4.0
        self.addSubview(labelBackdrop)
        
        if titleLabel == nil {
            createTitleLabel()
        }
        
        if valueLabel == nil {
            createValueLabel()
        }
        
        guard let
            tLbl = titleLabel,
            let vLbl = valueLabel
            else {
                assertionFailure("Why don't both labels exist at this point?")
                return
        }
        vLbl.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        vLbl.centerYAnchor.constraint(equalTo: self.centerYAnchor).isActive = true
        tLbl.centerXAnchor.constraint(equalTo: self.centerXAnchor).isActive = true
        tLbl.bottomAnchor.constraint(equalTo: vLbl.topAnchor).isActive = true
        
        
        self.labelBackdrop = labelBackdrop
    }
    
    private func createValueLabel() {
        let originalDescriptor = UIFont.systemFont(ofSize: 24.0, weight: UIFont.Weight.light).fontDescriptor
        let numericDescriptor = originalDescriptor.addingAttributes(
            [
                UIFontDescriptor.AttributeName.featureSettings : [
                    [
                        UIFontDescriptor.FeatureKey.featureIdentifier: kNumberSpacingType,
                        UIFontDescriptor.FeatureKey.typeIdentifier: kMonospacedNumbersSelector
                    ]
                ]
            ]
        )
        let vl = UILabel(frame: CGRect.zero)
        vl.contentMode = .center
        vl.font = UIFont(descriptor: numericDescriptor, size: 24.0)
        vl.translatesAutoresizingMaskIntoConstraints = false
        vl.isUserInteractionEnabled = false
        vl.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000.0), for: .horizontal)
        if autoHideValueLabel {
            vl.alpha = 0.0
            vl.isHidden = true
        }
        self.addSubview(vl)
        valueLabel = vl
    }
    
    /// Create the title label and position it within the control.
    private func createTitleLabel() {
        let tl = UILabel(frame: CGRect.zero)
        tl.translatesAutoresizingMaskIntoConstraints = false
        tl.setContentCompressionResistancePriority(UILayoutPriority(rawValue: 1000.0), for: .horizontal)
        tl.textAlignment = .center
        tl.font = UIFont.systemFont(ofSize: 14.0, weight: UIFont.Weight.regular)
        titleLabel = tl
        self.addSubview(tl)
    }
    
    /** Git along now, bossy! You git! */
    private func startLabelTimer() {
        self.autoHideTimer?.invalidate()
        self.autoHideTimer = Timer.scheduledTimer(timeInterval: fadeOutTime, target: self, selector: #selector(autoHideTimerDidFire(_:)), userInfo: nil, repeats: false)
    }
    
    @objc private func autoHideTimerDidFire(_ timer : Timer) {
        self.hideValueLabel()
    }
    
    override open func tintColorDidChange() {
        super.tintColorDidChange()
        self.valueLabel?.textColor = tintColor
    }
    
    //MARK: - Indicator
    
    private func createIndicator() {
        isHorizontal = self.bounds.width >= self.bounds.height
        let strokeWidth : CGFloat = 1.0 / UIScreen.main.scale
        let indicator : UIView
        if isHorizontal {
            indicator = UIView(frame: CGRect(x: 0.0, y: 0.0, width: strokeWidth, height: self.bounds.height))
        } else {
            indicator = UIView(frame: CGRect(x: 0.0, y: 0.0, width: self.bounds.width, height: strokeWidth))
        }
        indicator.backgroundColor = self.tintColor.withAlphaComponent(0.5)
        indicator.isUserInteractionEnabled = false
        indicator.alpha = 0.4
        self.addSubview(indicator)
        self.indicator = indicator
    }
    
    private func showIndicator() {
        UIView.animate(withDuration: 0.3, animations: {
            self.indicator?.alpha = 1.0
        }) 
    }
    
    private func hideIndicator() {
        UIView.animate(withDuration: 0.3, animations: {
            self.indicator?.alpha = 0.4
        }) 
    }
    
    private func setIndicatorPositionForPoint(_ point:CGPoint) {
        let lineWidth = 1.0 / UIScreen.main.scale
        if isHorizontal {
            let x = max(0.0, min(point.x, self.bounds.width - lineWidth))
            self.indicator?.frame = CGRect(x: x, y: 0.0, width: lineWidth, height: self.bounds.height)
        } else {
            let y = max(0.0, min(point.y, self.bounds.height - lineWidth))
            self.indicator?.frame = CGRect(x: 0.0, y: y, width: self.bounds.width, height: lineWidth)
        }
    }
    
    //MARK: - Touches
    
    override open func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        //Calcualte the axis of motion when touches start.
        if indicator == nil {
            self.createIndicator()
        }
        self.showIndicator()
        self.setValueForPoint(touch.location(in: self))
        return super.beginTracking(touch, with: event)
    }
    
    override open func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        self.setValueForPoint(touch.location(in: self))
        return super.continueTracking(touch, with: event)
    }
    
    override open func endTracking(_ touch: UITouch?, with event: UIEvent?) {
        self.hideIndicator()
        return super.endTracking(touch, with: event)
    }
    
    //MARK: - Value Interpolation
    
    func setValueForPoint(_ point:CGPoint) {
        let normalizedValue : CGFloat
        let touchInset = touchInsetPercent * (isHorizontal ? self.bounds.width : self.bounds.height)
        let twoInset = 2 * touchInset
        if isHorizontal {
            normalizedValue = max(0.0, min(point.x - touchInset, self.bounds.width - twoInset)) / (self.bounds.width - twoInset)
        } else {
            normalizedValue = max(0.0, min(point.y - touchInset, self.bounds.height - twoInset)) / (self.bounds.height - twoInset)
        }
        setIndicatorPositionForPoint(point)
        let v = (maximumValue - minimumValue) * normalizedValue + minimumValue
        let newValue = integralValues ? floor(v) : v
        if newValue == value {
            return
        }
        value = newValue
        sendActions(for: .valueChanged)
    }
    
    func calculatePrecision() {
        let delta = maximumValue - minimumValue
        guard delta > 0.0 else {
            numericPrecision = 0
            return
        }
        let deltaMagnitude = Int(log10(delta))
        numericPrecision = max(0, 3 - deltaMagnitude)
    }
    
    func calculateLabelBackdropFrame() -> CGRect? {
        guard let vLbl = valueLabel else { return nil }
        var baseRect = vLbl.frame
        if let tLbl = titleLabel {
            baseRect = baseRect.union(tLbl.frame)
        }
        baseRect = baseRect.insetBy(dx: -8.0, dy: -8.0)
        return baseRect
    }
}
