//
//  TouchCaptureControl.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 1/2/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage

class TouchCaptureControl: UIControl {
    var value: CIVector = CIVector(x: 0.0, y: 0.0)
    
    @IBInspectable var horizontalMinimumValue: CGFloat = 0.0
    @IBInspectable var horizontalMaximumValue: CGFloat = 1.0
    @IBInspectable var verticalMinimumValue: CGFloat = 0.0
    @IBInspectable var verticalMaximumValue: CGFloat = 1.0
    
    @IBInspectable var flipHorizontalAxis: Bool = false
    @IBInspectable var flipVerticalAxis: Bool = false
    
    //MARK: - Value Translation
    
    func setValueForPoint(_ point: CGPoint) {
        var normalizedX = point.x / bounds.width
        if flipHorizontalAxis { normalizedX = 1.0 - normalizedX }
        
        var normalizedY = point.y / bounds.height
        if flipVerticalAxis { normalizedY = 1.0 - normalizedY }
        
        let mappedX = (horizontalMaximumValue - horizontalMinimumValue) * normalizedX + horizontalMinimumValue
        let mappedY = (verticalMaximumValue - verticalMinimumValue) * normalizedY + verticalMinimumValue
        
        let newValue = CIVector(x: mappedX, y: mappedY)
        if newValue != value {
            value = newValue
            sendActions(for: .valueChanged)
        }
    }
    
    //MARK: - Touches
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        setValueForPoint(touch.location(in: self))
        return super.beginTracking(touch, with: event)
    }
    
    override func continueTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        setValueForPoint(touch.location(in: self))
        return super.continueTracking(touch, with: event)
    }
    
}


