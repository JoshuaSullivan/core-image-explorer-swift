//
//  AffineCaptureControl.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/28/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class AffineCaptureControl: UIControl {
    
    /// The value currently captured by the control.
    var value: CGAffineTransform {
        get {
            let aff = CGAffineTransform.identity
            let tAff = aff.translatedBy(x: tx, y: ty)
            let rAff = tAff.rotated(by: r)
            let sAff = rAff.scaledBy(x: s, y: s)
            return sAff
        }
        
        set {
            tx = newValue.tx
            ty = newValue.ty
            r = atan2(newValue.b, newValue.a)
            s = sqrt(newValue.a * newValue.a + newValue.c * newValue.c)
        }
    }
    
    private var tx: CGFloat = 0.0
    private var ty: CGFloat = 0.0
    private var r: CGFloat = 0.0
    private var s: CGFloat = 1.0
    private var lastTx: CGFloat = 0.0
    private var lastTy: CGFloat = 0.0
    private var lastR: CGFloat = 0.0
    private var lastS: CGFloat = 1.0
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        self.commonInit()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        self.commonInit()
    }
    
    private func commonInit() {
        let pan = UIPanGestureRecognizer(target: self, action: #selector(handlePan(_:)))
        self.addGestureRecognizer(pan)
        let rotate = UIRotationGestureRecognizer(target: self, action: #selector(handleRotate(_:)))
        self.addGestureRecognizer(rotate)
        let scale = UIPinchGestureRecognizer(target: self, action: #selector(handleScale(_:)))
        self.addGestureRecognizer(scale)
    }
    
    //MARK: - Handle gestures
    
    @objc private func handlePan(_ gesture: UIPanGestureRecognizer) {
        let t = gesture.translation(in: self)
        self.tx = lastTx + t.x
        self.ty = lastTy - t.y
        self.sendActions(for: .valueChanged)
    }
    
    @objc private func handleRotate(_ gesture: UIRotationGestureRecognizer) {
        let r = gesture.rotation
        self.r = lastR - r
        self.sendActions(for: .valueChanged)
    }
    
    @objc private func handleScale(_ gesture: UIPinchGestureRecognizer) {
        let s = gesture.scale
        self.s = lastS * s
        self.sendActions(for: .valueChanged)
    }
    
    override func beginTracking(_ touch: UITouch, with event: UIEvent?) -> Bool {
        lastTx = tx
        lastTy = ty
        lastR = r
        lastS = s
        
        return super.beginTracking(touch, with: event)
    }
}
