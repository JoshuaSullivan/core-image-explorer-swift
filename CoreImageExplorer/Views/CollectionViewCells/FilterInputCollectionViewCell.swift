//
//  FilterInputCollectionViewCell.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/13/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage

class FilterInputCollectionViewCell: UICollectionViewCell {
    
    static let identifier = "FilterInputCellIdentifier"
    
    var inputName: String! {
        didSet {
            nameLabel.text = inputName
        }
    }
    
    var attributes: NSDictionary! {
        didSet {
            guard let inputClass = attributes[kCIAttributeClass] as? String else {
                assertionFailure("Couldn't get input class!")
                return
            }
            let iconName: String
            switch inputClass {
            case "CIColor":     iconName = "Input-Color"
            case "CIImage":     iconName = "Input-Image"
            case "CIVector":    iconName = "Input-Vector"
            case "NSData":      iconName = "Input-Data"
            case "NSNumber":    iconName = "Input-Number"
            case "NSObject":    iconName = "Input-Object"
            case "NSString":    iconName = "Input-String"
            case "NSValue":     iconName = "Input-Value"
            default:
                print("WARNING: Unrecognized input class '\(inputClass)' found.")
                iconName = "Input-Object"
            }
            self.iconName = iconName
            imageView?.image = UIImage(named:iconName)
        }
    }

    var iconName: String?
    
    @IBOutlet weak var nameLabel: UILabel!

    @IBOutlet weak var imageView: UIImageView! {
        didSet {
            if let icon = iconName {
                imageView.image = UIImage(named: icon)
            }
        }
    }
    
    var enabled: Bool = true {
        didSet {
            self.imageView.alpha = enabled ? 1.0 : 0.5
            self.nameLabel.alpha = enabled ? 1.0 : 0.5
        }
    }
}
