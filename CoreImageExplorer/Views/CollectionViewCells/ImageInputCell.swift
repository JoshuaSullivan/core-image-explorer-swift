//
//  ImageInputCell.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 1/30/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

protocol ImageInputCellDelegate: class {
    func imageInputCellChangeCamera(_ cell: ImageInputCell)
}

class ImageInputCell: UICollectionViewCell {
    
    @IBOutlet private weak var imageView: UIImageView! {
        didSet {
            imageView.contentMode = .center
            imageView.layer.cornerRadius = 60.0
            imageView.clipsToBounds = true
            imageView.layer.borderColor = UIColor.yellow.cgColor
            imageView.layer.borderWidth = 2.0
        }
    }
    
    @IBOutlet private weak var caption: UILabel!

    @IBOutlet private weak var cameraCycleButton: UIButton! {
        didSet {
            let bounds = cameraCycleButton.bounds
            cameraCycleButton.layer.backgroundColor = UIColor.yellow.cgColor
            cameraCycleButton.layer.cornerRadius = bounds.width / 2.0
            cameraCycleButton.clipsToBounds = true
            cameraCycleButton.tintColor = .black
        }
    }

    weak var delegate: ImageInputCellDelegate?

    deinit {
        VideoProvider.shared.remove(observer: self)
    }

    override func prepareForReuse() {
        super.prepareForReuse()
        VideoProvider.shared.remove(observer: self)
        cameraCycleButton.isHidden = true
    }
    
    func configureWithImageType(_ imageType: ImageType, index: Int) {
        cameraCycleButton.isHidden = true
        ImageManager.sharedManager.getThumbnailImage(imageType, index: index) {
            [weak self]
            optionalImage in
            guard let strongSelf = self else { return }
            guard let image = optionalImage else {
                assertionFailure("Failed to get an image for type \(imageType) with index \(index).")
                return
            }
            strongSelf.imageView.image = image
        }
        self.caption.text = ImageManager.sharedManager.displayNameForImageType(imageType, index: index)
    }

    func configureForVideo(index: Int) {
        VideoProvider.shared.add(observer: self)
        cameraCycleButton.isHidden = false
        self.caption.text = NSLocalizedString("input.image.video", value: "Video", comment: "Label for the video input type.")
    }

    @IBAction private func changeCameraTapped(sender: UIButton?) {
        delegate?.imageInputCellChangeCamera(self)
    }
}

extension ImageInputCell: VideoProviderObserver {
    func videoProvider(_ videoProvider: VideoProvider, gotFrame ciImage: CIImage) {
        let imageSize = ciImage.extent.size
        let targetSize = self.bounds.size
        let scale = max(targetSize.width / imageSize.width, targetSize.height / imageSize.height)
        let transform = CGAffineTransform(scaleX: scale, y: scale)
        let scaledImage = ciImage.transformed(by: transform)
        RenderService.shared.renderToBitmap(image: scaledImage) { [weak self] result in
            guard
                let `self` = self,
                case .success(let image) = result
            else { return }
            self.imageView.image = UIImage(cgImage: image)
        }
    }
}
