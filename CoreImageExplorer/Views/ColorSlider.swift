//
//  ColorSlider.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/27/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage

class ColorSlider: UIControl {
    
    var color: CIColor = CIColor(red: 1.0, green: 1.0, blue: 1.0) {
        didSet {
            redSlider?.value = color.red
            greenSlider?.value = color.green
            blueSlider?.value = color.blue
            alphaSlider?.value = color.alpha
        }
    }
    
    private var redSlider: MinimalistSlider! {
        didSet {
            redSlider.tintColor = UIColor.red
            redSlider.value = color.red
            redSlider.title = NSLocalizedString("Red", comment: "Red color component.")
        }
    }
    
    private var greenSlider: MinimalistSlider! {
        didSet {
            greenSlider.tintColor = UIColor.green
            greenSlider.value = color.green
            greenSlider.title = NSLocalizedString("Green", comment: "Green color component.")
        }
    }
    
    private var blueSlider:  MinimalistSlider! {
        didSet {
            blueSlider.tintColor = UIColor.blue
            blueSlider.value = color.blue
            blueSlider.title = NSLocalizedString("Blue", comment: "Blue color component.")
        }
    }
    
    private var alphaSlider: MinimalistSlider! {
        didSet {
            alphaSlider.tintColor = UIColor.white
            alphaSlider.value = color.alpha
            alphaSlider.title = NSLocalizedString("Alpha", comment: "Alpha color component.")
        }
    }
    
    private var sliders: [MinimalistSlider] = []
    
    private var sliderConstraints: [NSLayoutConstraint] = []
    
    override func awakeFromNib() {
        super.awakeFromNib()
        createSliders()
    }
    
    func createSliders() {
        redSlider = MinimalistSlider()
        greenSlider = MinimalistSlider()
        blueSlider = MinimalistSlider()
        alphaSlider = MinimalistSlider()
        sliders = [redSlider, greenSlider, blueSlider, alphaSlider]
        sliders.forEach{
            $0.addTarget(self, action: #selector(ColorSlider.sliderValueChanged(_:)), for: .valueChanged)
            $0.translatesAutoresizingMaskIntoConstraints = false
            self.addSubview($0)
        }
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        sliderConstraints.forEach{ $0.isActive = false }
        sliderConstraints.removeAll()
        var previousSlider: MinimalistSlider? = nil
        if self.bounds.width < self.bounds.height {
            for slider in sliders {
                sliderConstraints.append(slider.leadingAnchor.constraint(equalTo: self.leadingAnchor))
                sliderConstraints.append(slider.trailingAnchor.constraint(equalTo: self.trailingAnchor))
                if let prevSlider = previousSlider {
                    sliderConstraints.append(slider.topAnchor.constraint(equalTo: prevSlider.bottomAnchor))
                    sliderConstraints.append(slider.heightAnchor.constraint(equalTo: prevSlider.heightAnchor))
                } else {
                    sliderConstraints.append(slider.topAnchor.constraint(equalTo: self.topAnchor))
                }
                previousSlider = slider
            }
            sliderConstraints.append(sliders.last!.bottomAnchor.constraint(equalTo: self.bottomAnchor))
        } else {
            for slider in sliders {
                sliderConstraints.append(slider.topAnchor.constraint(equalTo: self.topAnchor))
                sliderConstraints.append(slider.bottomAnchor.constraint(equalTo: self.bottomAnchor))
                if let prevSlider = previousSlider {
                    sliderConstraints.append(slider.leadingAnchor.constraint(equalTo: prevSlider.trailingAnchor))
                    sliderConstraints.append(slider.widthAnchor.constraint(equalTo: prevSlider.widthAnchor))
                } else {
                    sliderConstraints.append(slider.leadingAnchor.constraint(equalTo: self.leadingAnchor))
                }
                previousSlider = slider
            }
            sliderConstraints.append(sliders.last!.trailingAnchor.constraint(equalTo: self.trailingAnchor))
        }
        sliderConstraints.forEach{ $0.isActive = true }
    }
    
    @objc private func sliderValueChanged(_ slider: MinimalistSlider) {
        let r = redSlider.value
        let g = greenSlider.value
        let b = blueSlider.value
        let a = alphaSlider.value
        self.color = CIColor(red: r, green: g, blue: b, alpha: a)
        sendActions(for: .valueChanged)
    }
}
