//
//  FilterModificationViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/27/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

protocol FilterModificationDelegate: class {
    func filterModificationViewControllerUpdatedValue(_ filterModificationViewController: FilterModificationViewController)
    
    func filterModificationViewController(_ filterModificationViewController: FilterModificationViewController, requestsVideoForInput input: String)

    func filterModifiactionViewController(_ filterModificationViewController: FilterModificationViewController, stopVideoForInput input: String)
}

class FilterModificationViewController: UIViewController {
    
    private struct Constants {
        static let embedNavControllerSegueIdentifier = "embedNavControllerSegueIdentifier"
    }
    
    var filter: CIFilter!
    
    var delegate: FilterModificationDelegate?
    
    private var filterModifications: AttributeDictionary = [:]
    
    /// The background blur layer.
    @IBOutlet private weak var blurView: UIVisualEffectView!
    
    /// Navigation controller that allows transitions to the various control inputs.
    private weak var navController: UINavigationController! {
        didSet {
            navController.delegate = self
        }
    }
    
    private weak var inputBrowserController: FilterInputBrowserViewController! {
        didSet {
            inputBrowserController.delegate = self
        }
    }
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier else { return }
        
        if segueId == Constants.embedNavControllerSegueIdentifier {
            guard let
                navVC = segue.destination as? UINavigationController,
                let browserVC = navVC.topViewController as? FilterInputBrowserViewController
                else {
                    assertionFailure("Unexpected view controller structure encountered.")
                    return
            }
            self.navController = navVC
            browserVC.filter = self.filter
            self.inputBrowserController = browserVC
        }
    }
    
    //MARK: - Filter Modifications
    
    func applyModificationsToFilter() {
        filterModifications.keys.forEach {
            filter.setValue(filterModifications[$0], forKey: $0)
        }
    }
}

extension FilterModificationViewController: UINavigationControllerDelegate {
    func navigationController(_ navigationController: UINavigationController, animationControllerFor operation: UINavigationControllerOperation, from fromVC: UIViewController, to toVC: UIViewController) -> UIViewControllerAnimatedTransitioning? {
        
        let showingInput = toVC != inputBrowserController
        self.blurView.isHidden = showingInput
        
        //TODO: Add custom animation to the navigation controller transitions.
        return nil
    }
}

extension FilterModificationViewController: FilterInputBrowserDelegate {
    func filterInputBrowser(_ filterInputBrowser: FilterInputBrowserViewController, willShowInputModifier inputModifier: FilterInputModifier) {
        var mutableModifier = inputModifier
        mutableModifier.delegate = self
    }
}

extension FilterModificationViewController: FilterInputModifierDelegate {
    func filterInputModifier(_ modifier: FilterInputModifier, didSetInput input: String, toValue value: Any) {
        self.filterModifications[input] = value
        delegate?.filterModificationViewControllerUpdatedValue(self)
    }
    
    func filterInputModifier(_ modifier: FilterInputModifier, didRequestVideoForInput input: String) {
        delegate?.filterModificationViewController(self, requestsVideoForInput: input)
    }

    func filterInputModifier(_ modifier: FilterInputModifier, didStopVideoForInput input: String) {
        delegate?.filterModifiactionViewController(self, stopVideoForInput: input)
    }
}
