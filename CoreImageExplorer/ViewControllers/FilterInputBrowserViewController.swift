//
// Created by Joshua Sullivan on 12/13/15.
// Copyright (c) 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

protocol FilterInputBrowserDelegate {
    func filterInputBrowser(_ filterInputBrowser: FilterInputBrowserViewController, willShowInputModifier inputModifier: FilterInputModifier)
}

class FilterInputBrowserViewController: UIViewController {
    
    private struct InputClasses {
        static let CIColor  = "CIColor"
        static let CIImage  = "CIImage"
        static let CIVector = "CIVector"
        static let NSData   = "NSData"
        static let NSNumber = "NSNumber"
        static let NSObject = "NSObject"
        static let NSString = "NSString"
        static let NSValue  = "NSValue"
    }
    
    private struct SegueIdentifiers {
        static let NSNumber = "scalarInputSegueIdentifier"
        static let CIColor  = "colorInputSegueIdentifier"
        static let Location = "locationInputSegueIdentifier"
        static let Message = "messageInputSegueIdentifier"
        static let CIImage = "imageInputSegueIdentifier"
        static let AffineTransform = "affineInputSegueIdentifier"
        static let QRCorrection = "qrCorrectionInputSegueIdentifier"
        static let ColorSpace = "colorSpaceInputSegueIdentifier"
        static let ColorCube = "colorCubeInputSegueIdentifier"
        static let GenericVector = "genericVectorInputSegueIdentifier"
    }
    
    private struct ErrorMessages {
        static let unsupportedInput = "That input isn't supported just yet."
        static let colorCubeDimension = "This must match the size of the cube data or the filter breaks. It is set automatically."
        static let convolution = "Support for convolutions is coming soon."
    }
    
    /// The collection view to display the inputs.
    @IBOutlet private weak var collectionView: UICollectionView! {
        didSet {
            let screenWidth = UIScreen.main.bounds.width
            let spacing = self.calculateCellSpacingForViewWidth(screenWidth)
            setCollectionViewSpacing(spacing)
        }
    }
    
    @IBOutlet private weak var titleLabel: UILabel! {
        didSet {
            if let filterName = filter?.name {
                titleLabel.text = CIFilter.localizedName(forFilterName: filterName)
            }
        }
    }
    
    /// The filter to work with.
    var filter: CIFilter! {
        didSet {
            titleLabel?.text = CIFilter.localizedName(forFilterName: filter.name)
        }
    }
    
    /// The input browser's delegate.
    var delegate: FilterInputBrowserDelegate?
    
    private var selectedInputName: String = ""
    private var selectedInputAttributes: AttributeDictionary = [ : ]
    
    @IBOutlet private weak var notificationView: UIView! {
        didSet {
            notificationView.layer.cornerRadius = 4.0
        }
    }
    
    @IBOutlet private weak var notificationLabel: UILabel!
    
    private var notificationTimer: Timer?
    
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let segueId = segue.identifier else { return }
        switch segueId {
        case SegueIdentifiers.NSNumber,
             SegueIdentifiers.CIColor,
             SegueIdentifiers.Location,
             SegueIdentifiers.Message,
             SegueIdentifiers.CIImage,
             SegueIdentifiers.AffineTransform,
             SegueIdentifiers.QRCorrection,
             SegueIdentifiers.ColorSpace,
             SegueIdentifiers.ColorCube,
             SegueIdentifiers.GenericVector:
            guard var vc = segue.destination as? FilterInputModifier else {
                assertionFailure("Couldn't cast input.")
                return
            }
            vc.inputName = selectedInputName
            vc.inputAttributes = selectedInputAttributes
            if let initialValue = filter.value(forKey: selectedInputName) {
                vc.initialValue = initialValue as AnyObject!
            }
            delegate?.filterInputBrowser(self, willShowInputModifier: vc)
        default:
            return
        }
    }
    
    //MARK: - Styling
    
    override var prefersStatusBarHidden : Bool {
        return true
    }
    
    //MARK: - IBActions
    
    @IBAction private func doneTapped(_ sender: AnyObject?) {
        self.presentingViewController?.dismiss(animated: true, completion: nil)
    }
    
    @IBAction func unwindToInputBrowser(_ segue: UIStoryboardSegue) {
        // Do I need anything here?
    }
    
    //MARK: - Orientation Changes
    
    override func viewWillTransition(to size: CGSize, with coordinator: UIViewControllerTransitionCoordinator) {
        let spacing = calculateCellSpacingForViewWidth(size.width)
        setCollectionViewSpacing(spacing)
    }
    
    func calculateCellSpacingForViewWidth(_ width:CGFloat) -> CGFloat {
        let minimumSpacing: CGFloat = 12.0
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            print("Unexpected layout encountered. Aborting.")
            return minimumSpacing
        }
        let cellWidth = layout.itemSize.width
        var cols = floor(width / cellWidth)
        var spacing: CGFloat = 0.0
        repeat {
            let leftoverSpace = width - (cols * cellWidth)
            spacing = floor(leftoverSpace / (cols + 1.0))
            cols -= 1.0
        } while spacing < minimumSpacing && cols > 0.0
        return spacing
    }
    
    func setCollectionViewSpacing(_ spacing: CGFloat) {
        guard let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout else {
            print("Unexpected layout. No modification will be made.")
            return
        }
        
        layout.minimumInteritemSpacing = spacing
        layout.sectionInset = UIEdgeInsets(top: 0.0, left: spacing, bottom: 8.0, right: spacing)
        layout.minimumInteritemSpacing = spacing
        layout.minimumLineSpacing = 8.0
    }
    
    //MARK: - Input Selection
    
    func showInputControllerForInput(_ inputName: String) {
        guard let
            inputAttributes = filter.attributes[inputName] as? AttributeDictionary,
            let inputClass = inputAttributes[kCIAttributeClass] as? String
            else {
                assertionFailure("Couldn't get the input's class.")
                return
        }
        selectedInputName = inputName
        selectedInputAttributes = inputAttributes
        switch inputClass {
        case InputClasses.NSNumber:
            self.performSegue(withIdentifier: SegueIdentifiers.NSNumber, sender: self)
        case InputClasses.CIColor:
            self.performSegue(withIdentifier: SegueIdentifiers.CIColor, sender: self)
        case InputClasses.CIVector:
            determineAppropriateVectorInput(inputName, inputAttributes: inputAttributes)
        case InputClasses.NSData:
            determineAppropriateDataInput(inputName, inputAttributes: inputAttributes)
        case InputClasses.CIImage:
            self.performSegue(withIdentifier: SegueIdentifiers.CIImage, sender: self)
        case InputClasses.NSValue:
            self.performSegue(withIdentifier: SegueIdentifiers.AffineTransform, sender: self)
        case InputClasses.NSString:
            self.performSegue(withIdentifier: SegueIdentifiers.QRCorrection, sender: self)
        case InputClasses.NSObject:
            self.performSegue(withIdentifier: SegueIdentifiers.ColorSpace, sender: self)
        default:
            print("Inputs of type '\(inputClass)' aren't implemented yet.")
            showNotification(ErrorMessages.unsupportedInput, duration: 2.0)
        }
    }
    
    private func determineAppropriateVectorInput(_ inputName: String, inputAttributes: AttributeDictionary) {
        guard let inputClass = inputAttributes[kCIAttributeClass] as? String , inputClass == InputClasses.CIVector else {
            assertionFailure("This function should only be called for an input with a class of CIVector.")
            return
        }
        if let inputType = inputAttributes[kCIAttributeType] as? String {
            switch (inputType) {
            case kCIAttributeTypePosition:
                performSegue(withIdentifier: SegueIdentifiers.Location, sender: self)
            case kCIAttributeTypeOffset:
                performSegue(withIdentifier: SegueIdentifiers.GenericVector, sender: self)
            case kCIAttributeTypePosition3:
                performSegue(withIdentifier: SegueIdentifiers.GenericVector, sender: self)
            default:
                print("The input type '\(inputType)' isn't supported yet.")
                showNotification(ErrorMessages.unsupportedInput, duration: 2.0)
                return
            }
        } else {
            // No input type! These are almost all polynomial coefficients.
            if inputName != "inputWeights" {
                performSegue(withIdentifier: SegueIdentifiers.GenericVector, sender: self)
            } else {
                print("Convolutions aren't ready yet.")
                showNotification(ErrorMessages.convolution, duration: 2.0)
            }
            return
        }
    }
    
    private func determineAppropriateDataInput(_ inputName: String, inputAttributes: AttributeDictionary) {
        if (inputName == "inputMessage") {
            // This is a string input.
            self.performSegue(withIdentifier: SegueIdentifiers.Message, sender: self)
        } else if inputName == "inputCubeData" {
            // It's a color cube!
            self.performSegue(withIdentifier: SegueIdentifiers.ColorCube, sender: self)
        } else {
            showNotification(ErrorMessages.unsupportedInput, duration: 2.0)
        }
    }
    
    //MARK: - Error Messaging
    
    private func showNotification(_ message: String, duration: CFTimeInterval) {
        self.notificationView.alpha = 0.0
        self.notificationView.isHidden = false
        self.notificationLabel.text = message
        UIView.animate(withDuration: 0.4, animations: {
            self.notificationView.alpha = 1.0
        }) 
        notificationTimer?.invalidate()
        notificationTimer = Timer.scheduledTimer(timeInterval: duration, target: self, selector: #selector(notificationTimerDidFire(_:)), userInfo: nil, repeats: false)
    }
    
    @objc private func notificationTimerDidFire(_ timer: Timer) {
        notificationTimer = nil
        UIView.animate(withDuration: 0.2, animations: {
            self.notificationView.alpha = 0.0
            }, completion: {
                completed in
                self.notificationView.isHidden = true
        })
    }
}

extension FilterInputBrowserViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return filter.inputKeys.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: FilterInputCollectionViewCell.identifier, for: indexPath) as! FilterInputCollectionViewCell
        let key = filter.inputKeys[indexPath.item]
        let attributes = filter.attributes[key] as! NSDictionary
        if let inputName = attributes[kCIAttributeDisplayName] as? String {
            cell.inputName = inputName.stringBySplittingCamelCase()
        } else {
            cell.inputName = NSLocalizedString("unknown_filter_input", value: "Unknown", comment: "The default string to be presented when a filter input does not have a display name.")
        }
        cell.attributes = attributes
        if key == "inputCubeDimension" {
            cell.enabled = false
        }
        return cell
    }
}

extension FilterInputBrowserViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let key = filter.inputKeys[indexPath.item]
        showInputControllerForInput(key)
    }
    
    func collectionView(_ collectionView: UICollectionView, shouldSelectItemAt indexPath: IndexPath) -> Bool {
        let key = filter.inputKeys[indexPath.item]
        if key == "inputCubeDimension" {
            showNotification(ErrorMessages.colorCubeDimension, duration: 2.0)
            return false
        }
        return true
    }
}
