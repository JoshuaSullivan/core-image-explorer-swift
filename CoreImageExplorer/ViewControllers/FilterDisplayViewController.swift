//
//  FilterDisplayViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 7/16/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage
import Metal

class FilterDisplayViewController: UIViewController {

    private let showInputSegueIdentifier = "showInputSegueIdentifier"
    
    //MARK: - IBOutlets
    
    @IBOutlet private weak var imageView: UIImageView!
    
    private var totalImageInputs: Int = 0
    private var readyImageInputs: Int = 0
    private var hideTopBars: Bool = false {
        didSet {
            navigationController?.setNavigationBarHidden(hideTopBars, animated: true)
            setNeedsStatusBarAppearanceUpdate()
        }
    }
    
    private var pipelineIsReady: Bool {
        return totalImageInputs == readyImageInputs && self.imageView != nil
    }
            
    private var fromRect = UIScreen.main.nativeBounds
    private var toRect = UIScreen.main.nativeBounds
    
    private var filterNeedsUpdate: Bool = false
    private var renderInProgress: Bool = false
    
    private weak var filterModificationController: FilterModificationViewController?

    private var videoIsRunning: Bool = false

    private var videoBindings: [String] = [] {
        didSet {
            if videoIsRunning && videoBindings.isEmpty {
                VideoProvider.shared.remove(observer: self)
                videoIsRunning = false
                stopRecordingRenderingHistory()
            } else if !videoIsRunning && !videoBindings.isEmpty {
                VideoProvider.shared.add(observer: self)
                videoIsRunning = true
                startRecordingRenderingHistory()
            }
        }
    }

    private var renderingHistory: [Double] = []
    private var renderingHistoryIndex: Int = 0
    private var renderingHistoryTimer: Timer?
    
    //MARK: - Instance Variables
    
    var filterIdentifier: String! = "" {
        didSet {
            // Update the view.
            guard let filter = CIFilter(name: filterIdentifier) else {
                assertionFailure("Unable to instantiate filter with id: \(filterIdentifier ?? "UNKNOWN")")
                return
            }
            FilterAestheticsManager.sharedManager().configureFilter(filter)
            self.filter = filter
            shouldNormalizeOutputImage = FilterAestheticsManager.sharedManager().shouldNormalizeOutputForFilter(self.filterIdentifier)
        }
    }
    
    private var filter : CIFilter! {
        didSet {
            mapDefaultImageInputs(filter)
        }
    }
    
    private var hideShowTapGesture: UITapGestureRecognizer!
    
    private var shouldNormalizeOutputImage = true
    
    //MARK: - View Lifecycle

    override func viewDidLoad() {
        super.viewDidLoad()
        self.navigationItem.title = CIFilter.localizedName(forFilterName: filterIdentifier)
        hideShowTapGesture = UITapGestureRecognizer(target: self, action: #selector(handleTap(_:)))
        hideShowTapGesture.delegate = self
        self.view.addGestureRecognizer(hideShowTapGesture)
        self.view.backgroundColor = UIColor(patternImage: UIImage(named: "BackgroundPattern-Dark")!)
        self.view.parent(otherView: RenderService.shared.renderingSurface)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.hideTopBars = false
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        redrawFilter()
    }

    override func viewDidDisappear(_ animated: Bool) {
        super.viewDidDisappear(animated)
        videoBindings = []
        RenderService.shared.clearDisplay()
    }
        
    //MARK: - Navigation
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        guard let id = segue.identifier else { return }
        switch id {
        case showInputSegueIdentifier:
            guard let inputVC = segue.destination as? FilterModificationViewController else {
                assertionFailure("Unexpected view controller encountered.")
                return
            }
            inputVC.delegate = self
            inputVC.filter = filter
            filterModificationController = inputVC
            self.hideTopBars = true
        default:
            return
        }
    }
    
    //MARK: - Filter Drawing
    
    private func redrawFilter() {
        // If the context and filter aren't ready, don't render.
        guard pipelineIsReady else { return }
        
        // If a render is already in progress, don't render.
        guard !renderInProgress else { return }
        
        // Insure we have an image to render.
        guard var image = filter?.outputImage else {
            assertionFailure("ERROR: Failed to get output image from filter: \(filter?.name ?? "Unknown Filter")")
            return
        }
        renderInProgress = true
        
        // Perform the rendering off the main queue so we don't block the UI.
        DispatchQueue.global(qos: .default).async {
            // Some filters (like the bar code generators) produce tiny output. Scale it up!
            var renderRect = self.fromRect
            if self.shouldNormalizeOutputImage && image.extent.width < self.fromRect.width && image.extent.height < self.fromRect.height {
                    let scaleX = self.fromRect.width / image.extent.width
                    let scaleY = self.fromRect.height / image.extent.height
                    let scale = min(scaleX, scaleY)
                    let scaleTransform = CGAffineTransform(scaleX: scale, y: scale)
                    
                    image = image.transformed(by: scaleTransform)
                    renderRect = renderRect.intersection(image.extent)
            }

            // Render the image to the display surface, tracking its rendering time.
            let start = CACurrentMediaTime()
            RenderService.shared.renderToDisplay(image: image, from: renderRect, to: self.toRect)
            let end = CACurrentMediaTime()
            self.recordRenderingTime(end - start)
            

            DispatchQueue.main.async {
                self.renderInProgress = false
                if self.filterNeedsUpdate {
                    self.applyFilterUpdatesAndRender()
                }
            }
        }
    }
    
    private func applyFilterUpdatesAndRender() {
        guard filterNeedsUpdate else { return }
        guard let modVC = filterModificationController else { return }
        modVC.applyModificationsToFilter()
        filterNeedsUpdate = false
        redrawFilter()
    }
    //MARK: - Prepare a filter
    
    func mapDefaultImageInputs(_ filter:CIFilter) {
        filter.inputKeys.forEach { key in
            var sampleType: ImageType?
            var index: Int = 0
            switch key {
            case kCIInputImageKey:
                sampleType = .image
            case kCIInputBackgroundImageKey, kCIInputTargetImageKey:
                sampleType = .image
                index = 1
            case kCIInputMaskImageKey:
                if filter.name == "CIMaskToAlpha" {
                    sampleType = .alphaMask
                } else {
                    sampleType = .grayscaleMask
                }
            case "inputBacksideImage":
                sampleType = .image
            case "inputShadingImage":
                sampleType = .grayscaleMask
            case "inputDisplacementImage":
                sampleType = .grayscaleMask
            case "inputGradientImage":
                sampleType = .image
                index = 1
            default:
                break
            }
            if let type = sampleType {
                totalImageInputs += 1
                assignImage(type, index: index, toFilter: filter, forInput: key)
            }
        }
    }
    
    private func assignImage(_ type: ImageType, index: Int, toFilter filter:CIFilter, forInput input: String) {
        ImageManager.sharedManager.getRenderingImageForCurrentOrientation(type, index: index) {
            [weak self] optionalImage in
            guard let strongSelf = self else { return }
            if let image = optionalImage {
                filter.setValue(image, forKey: input)
                strongSelf.readyImageInputs += 1
            } else {
                print("Failed to get image for input '\(input)' on filter \(filter.name). It will be left unset.")
                strongSelf.totalImageInputs -= 1
            }
            if (strongSelf.pipelineIsReady) {
                print("All inputs mapped. Ready to draw image:", filter)
                strongSelf.redrawFilter()
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction private func settingsTapped(_ sender: AnyObject?) {
        self.hideTopBars = true
        self.performSegue(withIdentifier: showInputSegueIdentifier, sender: self)
    }
    
    @objc @IBAction private func handleTap(_ gesture: UITapGestureRecognizer) {
        
        toggleTopBars()
        if pipelineIsReady {
            redrawFilter()
        }
    }

    private func startRecordingRenderingHistory() {
        renderingHistory = Array<Double>(repeating: 0.0, count: 20)
        renderingHistoryIndex = 0
        renderingHistoryTimer?.invalidate()
        renderingHistoryTimer = Timer.scheduledTimer(withTimeInterval: 0.5, repeats: true, block: { [weak self] timer in
            guard let `self` = self else {
                timer.invalidate()
                return
            }
            let sum = self.renderingHistory.reduce(0.0) { $0 + $1 }
            let average = sum / 20.0
            print("Average render time: \(Int(average * 1000))ms - (\(Int(1.0 / average)) fps)")
        })
    }

    private func stopRecordingRenderingHistory() {
        renderingHistoryTimer?.invalidate()
    }

    private func recordRenderingTime(_ time: TimeInterval) {
        guard videoIsRunning else { return }
        renderingHistory[renderingHistoryIndex] = time
        renderingHistoryIndex = (renderingHistoryIndex + 1) % 20
    }
    
    //MARK: - Status Bar
    
    func toggleTopBars() {
        hideTopBars = !hideTopBars
    }
    
    override var preferredStatusBarStyle : UIStatusBarStyle {
        return .lightContent
    }
    
    override var prefersStatusBarHidden : Bool {
        return hideTopBars
    }
    
    override var preferredStatusBarUpdateAnimation : UIStatusBarAnimation {
        return .slide
    }

    
    //MARK: - Video Input

    private func bindVideo(toInput input: String) {
        guard !videoBindings.contains(input) else { return }
        videoBindings.append(input)
    }

    private func removeVideo(fromInput input: String) {
        videoBindings = videoBindings.filter { $0 != input }
    }
}

extension FilterDisplayViewController: UIGestureRecognizerDelegate {
    func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldRecognizeSimultaneouslyWith otherGestureRecognizer: UIGestureRecognizer) -> Bool {
        return true
    }
}

extension FilterDisplayViewController: FilterModificationDelegate {
    func filterModificationViewControllerUpdatedValue(_ filterModificationViewController: FilterModificationViewController) {
        filterNeedsUpdate = true
        if !renderInProgress {
            applyFilterUpdatesAndRender()
        }
    }
    
    func filterModificationViewController(_ filterModificationViewController: FilterModificationViewController, requestsVideoForInput input: String) {
        bindVideo(toInput: input)
    }

    func filterModifiactionViewController(_ filterModificationViewController: FilterModificationViewController, stopVideoForInput input: String) {
        removeVideo(fromInput: input)
    }
}

extension FilterDisplayViewController: VideoProviderObserver {
    func videoProvider(_ videoProvider: VideoProvider, gotFrame ciImage: CIImage) {
        videoBindings.forEach { filter.setValue(ciImage, forKey: $0) }
        redrawFilter()
    }
}

