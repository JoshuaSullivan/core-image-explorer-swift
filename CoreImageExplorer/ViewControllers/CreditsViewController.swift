//
//  CreditsViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/30/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class CreditsViewController: UIViewController {
    
    @IBAction func twitterHandleTapped(_ sender: AnyObject?) {
        let url = URL(string: "https://twitter.com/ChibiJosh")!
        if UIApplication.shared.canOpenURL(url) {
            UIApplication.shared.openURL(url)
        }
    }
}
