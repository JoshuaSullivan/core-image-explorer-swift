//
//  FilterListViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 7/16/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit
import CoreImage

class FilterListViewController: UITableViewController {
    
    let kFilterListCellIdentifier = "kFilterListCell"
    
    var filterIdentifiers: [String] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.clearsSelectionOnViewWillAppear = true
        filterIdentifiers = CIFilter.filterNames(inCategory: kCICategoryBuiltIn)
    }

    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Segues

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "ListToDisplaySegueIdentifier" {
            if let indexPath = self.tableView.indexPathForSelectedRow {
                let filterIdentifier = filterIdentifiers[indexPath.row]
                let controller = segue.destination as! FilterDisplayViewController
                controller.filterIdentifier = filterIdentifier
            } else {
                assertionFailure("Cannot get index from table!")
            }
        }
    }
    
    @IBAction func unwindToFilterList(_ segue: UIStoryboardSegue) {
        // Do nothing.
    }
}

//MARK: - Data Source

extension FilterListViewController {

    override func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filterIdentifiers.count
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        let cell = tableView.dequeueReusableCell(withIdentifier: kFilterListCellIdentifier, for: indexPath)
        let filterIdentifier = filterIdentifiers[indexPath.row]
        cell.textLabel!.text = CIFilter.localizedName(forFilterName: filterIdentifier)
        return cell
    }
}



