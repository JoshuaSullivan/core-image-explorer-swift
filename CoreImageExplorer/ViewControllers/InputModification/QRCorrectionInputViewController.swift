//
//  QRCorrectionInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/29/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class QRCorrectionInputViewController: UIViewController, FilterInputModifier {
    
    private let values = ["L", "M", "Q", "H"]
    
    var inputName: String!
    
    var inputAttributes: AttributeDictionary!
    
    var initialValue: Any! {
        didSet {
            guard let val = initialValue as? String else {
                assertionFailure("initialValue wasn't a string as expected.")
                return
            }
            guard let index = values.index(of: val) else {
                assertionFailure("Initial value \(val) wasn't found in the list of possible values: \(values)")
                return
            }
            startingIndex = index
        }
    }
    
    private var startingIndex: Int = 0
    
    var delegate: FilterInputModifierDelegate?
    
    @IBOutlet private weak var control: UISegmentedControl! {
        didSet {
            control.selectedSegmentIndex = startingIndex
        }
    }
    
    @IBAction func controlValueChanged(_ control: UISegmentedControl) {
        let index = control.selectedSegmentIndex
        let value = values[index]
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: value)
    }
    
}
