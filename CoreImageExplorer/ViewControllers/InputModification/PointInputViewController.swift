//
//  PointInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 1/2/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class PointInputViewController: UIViewController, FilterInputModifier {
    
    var inputName: String!
    
    var inputAttributes: AttributeDictionary! {
        didSet {
            guard let
                inputClass = inputAttributes[kCIAttributeClass] as? String,
                let inputType = inputAttributes[kCIAttributeType] as? String else {
                    assertionFailure("Unable to find class and type in the attribute dictionary.")
                    return
            }
            if !(inputClass == "CIVector" && inputType == kCIAttributeTypePosition) {
                assertionFailure("The PointInputViewController can only handle inputs with a CIVector / CIAttributeTypePosition input type.")
                return
            }
        }
    }
    
    var initialValue: Any! {
        didSet {
            guard let startingPoint = initialValue as? CIVector else {
                assertionFailure("The initial value must be a CIVector.")
                return
            }
            initialPoint = startingPoint
        }
    }
    
    var initialPoint: CIVector = CIVector(x: 0.0, y: 0.0) {
        didSet {
            touchView?.value = initialPoint
        }
    }
    
    weak var delegate: FilterInputModifierDelegate?
    
    @IBOutlet weak var touchView: TouchCaptureControl! {
        didSet {
            touchView.value = initialPoint
            let scale = UIScreen.main.nativeScale
            touchView.horizontalMaximumValue = UIScreen.main.bounds.width * scale
            touchView.verticalMaximumValue = UIScreen.main.bounds.height * scale
        }
    }
    
    //MARK: - Touch Control
    
    @IBAction private func touchViewDidChange(_ touchView: TouchCaptureControl) {
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: touchView.value)
    }
}
