//
//  ColorCubeInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/29/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class ColorCubeInputViewController: UIViewController, FilterInputModifier {

    var inputName: String!
    
    var inputAttributes: AttributeDictionary!
    
    var initialValue: Any!
    
    var delegate: FilterInputModifierDelegate?
    
    private let  colorCubeIdentifiers = [
        "DefaultColorCube",
        "AcidTripColorCube",
        "HighContrastBWColorCube",
        "HotBlackColorCube",
        "JustBlueItColorCube",
        "NightVisionColorCube"
    ]
    
    private let colorCubeNames = [
        "Default (Unchanged)",
        "Acid Trip",
        "High Contrast B&W",
        "Hot Black",
        "Just Blue It",
        "Night Vision"
    ]
    
    private var cubeDataCache: [String : Data] = [:]
    
    @IBOutlet weak var picker: UIPickerView!
}

extension ColorCubeInputViewController: UIPickerViewDataSource {
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colorCubeIdentifiers.count
    }
}

extension ColorCubeInputViewController: UIPickerViewDelegate {
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let name = colorCubeNames[row]
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : UIColor.yellow
        ]
        let attributedName = NSAttributedString(string: name, attributes: attributes)
        return attributedName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let id = colorCubeIdentifiers[row]
        if let cubeData = cubeDataCache[id] {
            self.delegate?.filterInputModifier(self, didSetInput: inputName, toValue: cubeData)
            return
        }
        guard let cubeImage = UIImage(named: id) else {
            assertionFailure("Unable to find cube image named '\(id)'.")
            return
        }
        guard let cubeData = try? ColorCubeHelper.createColorCubeData(inputImage: cubeImage, cubeDimension: 64) else {
            assertionFailure("Unable to convert image to cube data.")
            return
        }
        cubeDataCache[id] = cubeData as Data
        self.delegate?.filterInputModifier(self, didSetInput: inputName, toValue: cubeData)
    }
}
