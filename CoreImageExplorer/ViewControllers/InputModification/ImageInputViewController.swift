//
//  ImageInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 1/24/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class ImageInputViewController: UIViewController, FilterInputModifier {

    var inputName: String!
    
    var inputAttributes: AttributeDictionary!
    
    /// This controller doesn't use the initial value object because of the difficulty in establishing equality between CIImages.
    var initialValue: Any!
    
    weak var delegate: FilterInputModifierDelegate?
    
    private var sourceCountArray: [Int] = []
    private var imageTypes: [ImageType] = []
    
    private var cellSpacing: CGFloat {
        var columns: CGFloat = 5.0
        var padding: CGFloat = 0.0
        repeat {
            padding = paddingForColumnCount(columns, columnWidth: 120.0)
            columns -= 1.0
        } while (padding < 8.0 && columns > 0.0)
        return floor(padding)
    }
    
    @IBOutlet private weak var collectionView: UICollectionView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        if inputName.lowercased().contains("mask") {
            imageTypes = [.grayscaleMask, .alphaMask, .image, .video]
        } else {
            imageTypes = [.image, .video, .grayscaleMask, .alphaMask]
        }
        
        for type in imageTypes {
            guard type != .video else {
                sourceCountArray.append(1)
                continue
            }
            sourceCountArray.append(ImageManager.sharedManager.getImageTypeCount(type))
        }
        
        if let layout = collectionView.collectionViewLayout as? UICollectionViewFlowLayout {
            let spacing = cellSpacing
            layout.minimumInteritemSpacing = spacing
            layout.sectionInset = UIEdgeInsets(top: 8.0, left: spacing, bottom: 8.0, right: spacing)
        }
    }
}

extension ImageInputViewController: UICollectionViewDataSource {
    func numberOfSections(in collectionView: UICollectionView) -> Int {
        let count = sourceCountArray.count
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        let count = sourceCountArray[section]
        return count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ImageCell", for: indexPath) as! ImageInputCell
        let type = imageTypes[indexPath.section]
        if type != .video {
            cell.configureWithImageType(type, index: indexPath.item)
        } else {
            cell.configureForVideo(index: indexPath.item)
        }
        cell.delegate = self
        return cell
    }
    
    private func paddingForColumnCount(_ columns: CGFloat, columnWidth: CGFloat) -> CGFloat {
        let usedWidth = columns * columnWidth
        let screenWidth = UIScreen.main.bounds.width
        let totalPadding = screenWidth - usedWidth
        let padding = totalPadding / (columns + 1.0)
        return padding
    }
}

extension ImageInputViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let imageType = imageTypes[indexPath.section]
        if imageType == .video {
            delegate?.filterInputModifier(self, didRequestVideoForInput: inputName)
        } else {
            returnImage(type: imageType, index: indexPath.item)
        }
    }

    private func returnImage(type: ImageType, index: Int) {
        delegate?.filterInputModifier(self, didStopVideoForInput: inputName)
        ImageManager.sharedManager.getRenderingImageForCurrentOrientation(type, index: index) {
            [weak self]
            optionalImage in
            guard let strongSelf = self else { return }
            guard let image = optionalImage else {
                assertionFailure("Unable to get rendering image for type \(type) with index \(index).")
                return
            }
            strongSelf.delegate?.filterInputModifier(strongSelf, didSetInput: strongSelf.inputName, toValue: image)
        }
    }
}

extension ImageInputViewController: ImageInputCellDelegate {
    func imageInputCellChangeCamera(_ cell: ImageInputCell) {
        delegate?.filterInputModifierChangeCamera(self)
    }
}

