//
//  ColorSpaceInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/29/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class ColorSpaceInputViewController: UIViewController, FilterInputModifier {
    
    var inputName: String!
    
    var inputAttributes: AttributeDictionary!
    
    var initialValue: Any!
    
    var delegate: FilterInputModifierDelegate?
    
    @IBOutlet private var picker: UIPickerView!
    
    private let colorSpaceIdentifiers: [CFString] = {
        var names: [CFString] = [
            CGColorSpace.genericCMYK,
            CGColorSpace.genericRGBLinear,
            CGColorSpace.adobeRGB1998,
            CGColorSpace.sRGB,
            CGColorSpace.genericGrayGamma2_2,
            CGColorSpace.genericXYZ,
            CGColorSpace.acescgLinear,
            CGColorSpace.itur_709,
            CGColorSpace.itur_2020,
            CGColorSpace.rommrgb,
            CGColorSpace.dcip3
        ]
        if #available(iOS 9.3, *) {
            names.append(CGColorSpace.displayP3)
        }
        if #available(iOS 10.0, *) {
            names.append(CGColorSpace.extendedGray)
            names.append(CGColorSpace.extendedSRGB)
            names.append(CGColorSpace.extendedLinearGray)
            names.append(CGColorSpace.extendedLinearSRGB)
        }
        return names
    }()
    
    //TODO: Localization Support?
    private let colorSpaceNames: [String] = {
        
        var names: [String] = [
            "Device Gray",
            "Device RGB",
            "Device CMYK",
            "Generic CMYK",
            "Generic RGB (Linear)",
            "Adobe RGB 1998",
            "sRGB",
            "Generic Gray (Gamma 2.2)",
            "Generic XYZ",
            "ACEScg",
            "ITU-R BT.709",
            "ITU-R BT.2020",
            "ROMM RGB",
            "DCI P3"
        ]
        if #available(iOS 9.3, *) {
            names.append("Display P3")
        }
        if #available(iOS 10.0, *) {
            names.append("Extended Gray")
            names.append("Extended sRGB")
            names.append("Extended Linear Gray")
            names.append("Extended Linear sRGB")
        }
        return names
    }()
}

extension ColorSpaceInputViewController: UIPickerViewDataSource {
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 1
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return colorSpaceNames.count
    }
}

extension ColorSpaceInputViewController: UIPickerViewDelegate {
    
    func pickerView(_ pickerView: UIPickerView, attributedTitleForRow row: Int, forComponent component: Int) -> NSAttributedString? {
        let name = colorSpaceNames[row]
        let attributes: [NSAttributedStringKey : Any] = [
            NSAttributedStringKey.foregroundColor : UIColor.yellow
        ]
        let attributedName = NSAttributedString(string: name, attributes: attributes)
        return attributedName
    }
    
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let colorSpace: CGColorSpace
        if row == 0 {
            colorSpace = CGColorSpaceCreateDeviceGray()
        } else if row == 1 {
            colorSpace = CGColorSpaceCreateDeviceRGB()
        } else if row == 2 {
            colorSpace = CGColorSpaceCreateDeviceCMYK()
        } else {
            let index = row - 2
            let identifier = colorSpaceIdentifiers[index]
            guard let cs = CGColorSpace(name: identifier) else {
                assertionFailure("Unable to create color space with identifier: \(identifier)")
                return
            }
            colorSpace = cs
        }
        
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: colorSpace)
    }
}
