//
//  ColorInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/29/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

class ColorInputViewController: UIViewController, FilterInputModifier {
    @IBOutlet private weak var colorSlider: ColorSlider! {
        didSet {
            colorSlider.color = initialColor
            colorSlider.addTarget(self, action: #selector(sliderValueChanged(_:)), for: .touchUpInside)
        }
    }
    
    var inputName: String!
    var inputAttributes: AttributeDictionary!
    var initialValue: Any! {
        didSet {
            guard let color = initialValue as? CIColor else {
                assertionFailure("The initial value must be a CIColor!")
                return
            }
            self.initialColor = color
        }
    }
    private var initialColor: CIColor = CIColor(color: UIColor.white)
    weak var delegate: FilterInputModifierDelegate?
    
    @objc private func sliderValueChanged(_ slider: ColorSlider) {
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: slider.color)
    }
    
}
