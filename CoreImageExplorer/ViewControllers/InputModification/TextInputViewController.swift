//
//  TextInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 1/4/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class TextInputViewController: UIViewController, FilterInputModifier {
    
    var inputName: String!
    
    var inputAttributes: AttributeDictionary! {
        didSet {
            guard let inputClass = inputAttributes[kCIAttributeClass] as? String , inputClass == "NSData" else {
                assertionFailure("This input controller only works with NSData attributes.")
                return
            }
        }
    }
    
    var initialValue: Any! {
        didSet {
            guard let strData = initialValue as? Data else {
                assertionFailure("The initialValue must be NSData.")
                return
            }
            guard let str = String(data: strData, encoding: String.Encoding.utf8) else {
                assertionFailure("The data must contain a UTF-8 encoded string.")
                return
            }
            stringValue = str
        }
    }
    
    weak var delegate: FilterInputModifierDelegate?
    
    var stringValue: String = "" {
        didSet {
            guard let data = stringValue.data(using: String.Encoding.utf8) else {
                assertionFailure("Couldn't convert the string to data.")
                return
            }
            delegate?.filterInputModifier(self, didSetInput: inputName, toValue: data)
        }
    }
    
    @IBOutlet var inputField: UITextField! {
        didSet {
            inputField.delegate = self
            inputField.layer.cornerRadius = 4.0
            inputField.layer.borderColor = UIColor.yellow.cgColor
            inputField.layer.borderWidth = 1.0 / UIScreen.main.nativeScale
            inputField.clipsToBounds = true
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        inputField.becomeFirstResponder()
    }
}

extension TextInputViewController: UITextFieldDelegate {
    func textField(_ textField: UITextField, shouldChangeCharactersIn range: NSRange, replacementString string: String) -> Bool {
        let rawString = textField.text ?? ""
        guard let stringRange = rawString.range(from: range) else {
                assertionFailure("Could not create string range.")
                return true
        }
        let composedString = rawString.replacingCharacters(in: stringRange, with: string)
        stringValue = composedString
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return false
    }
}
