//
//  ScalarInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 12/27/15.
//  Copyright © 2015 Joshua Sullivan. All rights reserved.
//

import UIKit

class ScalarInputViewController: UIViewController, FilterInputModifier {
    
    /// The attribute dictionary for this input.
    var inputAttributes: AttributeDictionary! {
        didSet {
            guard let inputClass = inputAttributes[kCIAttributeClass] as? String
                , inputClass == "NSNumber" else {
                    assertionFailure("This input is not a scalar.")
                    return
            }
            self.displayName = inputAttributes[kCIAttributeDisplayName] as? String
        }
    }
    
    var inputName: String!
    
    var initialValue: Any! {
        didSet {
            guard let initialFloat = initialValue as? CGFloat else {
                assertionFailure("The initialValue must be a CGFloat!")
                return
            }
            self.initialFloat = initialFloat
        }
    }
    
    var initialFloat: CGFloat = 0.0
    
    var displayName: String?
    
    weak var delegate: FilterInputModifierDelegate?
    
    @IBOutlet private weak var slider: MinimalistSlider! {
        didSet {
            let min: CGFloat, max: CGFloat
            if let
                aMin = inputAttributes[kCIAttributeSliderMin] as? CGFloat,
                let aMax = inputAttributes[kCIAttributeSliderMax] as? CGFloat {
                min = aMin
                max = aMax
            } else {
                if inputName == "inputBias" {
                    min = 0
                    max = 255
                } else if inputName == "inputBottomHeight" {
                    min = 0
                    max = 200
                } else {
                    min = 0
                    max = 1
                }
            }
            slider.minimumValue = min
            slider.maximumValue = max
            slider.value = initialFloat
            slider.title = self.displayName
            
            guard let type = inputAttributes[kCIAttributeType] as? String else { return }
            if type == kCIAttributeTypeCount {
                slider.integralValues = true
            }
        }
    }
    
    //MARK: - IBActions
    
    @IBAction func sliderValueChanged(_ slider: MinimalistSlider) {
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: NSNumber(value: Double(slider.value) as Double))
    }
}
