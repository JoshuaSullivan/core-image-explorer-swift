//
//  GenericVectorInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/29/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class GenericVectorInputViewController: UIViewController, FilterInputModifier {
    
    var inputName: String!
    
    var inputAttributes: AttributeDictionary!
    
    var initialValue: Any! {
        didSet {
            guard let vec = initialValue as? CIVector else {
                assertionFailure("initialValue was not a CIVector.")
                return
            }
            startingVector = vec
            componentCount = vec.count
        }
    }
    
    var delegate: FilterInputModifierDelegate?
    
    private var startingVector: CIVector = CIVector()
    
    private var componentCount: Int = 0
    
    @IBOutlet private weak var stackView: UIStackView!
    
    private var sliders: [MinimalistSlider] = []
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        for index in 0..<componentCount {
            let componentValue = startingVector.value(at: index)
            let slider = MinimalistSlider(frame: CGRect.zero)
            configureSliderRange(slider, index: index)
            configureSliderLabel(slider, index: index)
            slider.value = componentValue
            slider.addTarget(self, action: #selector(sliderValueChanged(_:)), for: .valueChanged)
            stackView.addArrangedSubview(slider)
            sliders.append(slider)
        }
    }
    
    @objc private func sliderValueChanged(_ slider: MinimalistSlider) {
        let values = sliders.map { $0.value }
        let vec = CIVector(values: values, count: values.count)
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: vec)
        
    }
    
    private func configureSliderRange(_ slider: MinimalistSlider, index: Int) {
        switch inputName {
        case "inputNeutral", "inputTargetNeutral":
            if index == 0 {
                slider.minimumValue = 2000
                slider.maximumValue = 10000
                slider.integralValues = true
            } else {
                slider.minimumValue = -100.0
                slider.maximumValue = 100.0
            }
        case "inputShadowOffset":
            slider.minimumValue = -100
            slider.maximumValue = 100
        case "inputLightPosition", "inputLightPointsAt":
            let size = UIScreen.main.bounds.size
            let scale = UIScreen.main.nativeScale
            slider.minimumValue = 0
            if index == 0 {
                slider.maximumValue = size.width * scale
            } else if index == 1 {
                slider.maximumValue = size.height * scale
            } else {
                slider.maximumValue = 1000.0
            }
        default:
            slider.minimumValue = 0.0
            slider.maximumValue = 1.0
        }
    }
    
    private func configureSliderLabel(_ slider: MinimalistSlider, index: Int) {
        
        switch inputName {
        case "inputNeutral", "inputTargetNeutral":
            if index == 0 {
                slider.title = "Temperature"
            } else {
                slider.title = "Tint"
            }
        case "inputRedCoefficients", "inputGreenCoefficients", "inputBlueCoefficients", "inputAlphaCoefficients":
            if startingVector.count > 4 {
                slider.autoHideValueLabel = false
            } else {
                let titles = ["Red", "Green", "Blue", "Alpha"]
                let tintColors = [UIColor.red, UIColor.green, UIColor.blue, UIColor.white]
                slider.title = titles[index]
                slider.tintColor = tintColors[index]
            }
        case "inputShadowOffset":
            if index == 0 {
                slider.title = "X Offset"
            } else {
                slider.title = "Y Offset"
            }
        case "inputLightPosition", "inputLightPointsAt":
            if index == 0 {
                slider.title = "X"
            } else if index == 1 {
                slider.title = "Y"
            } else {
                slider.title = "Z"
            }
        default:
            return
        }
    }
}
