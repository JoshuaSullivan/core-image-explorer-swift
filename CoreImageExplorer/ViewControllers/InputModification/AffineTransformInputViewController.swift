//
//  AffineTransformInputViewController.swift
//  CoreImageExplorer
//
//  Created by Joshua Sullivan on 3/28/16.
//  Copyright © 2016 Joshua Sullivan. All rights reserved.
//

import UIKit

class AffineTransformInputViewController: UIViewController, FilterInputModifier {
    
    var inputName: String!
    
    var inputAttributes: AttributeDictionary!
    
    var initialValue: Any! {
        didSet {
            guard let val = initialValue as? NSValue else {
                assertionFailure("Unexpected initial value type.")
                return
            }
            startingTransform = val.cgAffineTransformValue
            if let ac = affControl {
                ac.value = startingTransform
            }
        }
    }
    
    private var startingTransform: CGAffineTransform = CGAffineTransform.identity
    
    weak var delegate: FilterInputModifierDelegate?
    
    @IBOutlet weak var affControl: AffineCaptureControl! {
        didSet {
            affControl.value = startingTransform
        }
    }
    
    @IBOutlet weak var instructions: UIView! {
        didSet {
            instructions.layer.cornerRadius = 4.0
        }
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        Timer.scheduledTimer(timeInterval: 3.0, target: self, selector: #selector(hideInstructionTimerDidFire(_:)), userInfo: nil, repeats: false)
    }
    
    @IBAction func handleAffControlValueChange(_ affControl: AffineCaptureControl) {
        let value = NSValue(cgAffineTransform: affControl.value)
        delegate?.filterInputModifier(self, didSetInput: inputName, toValue: value)
    }
    
    @objc private func hideInstructionTimerDidFire(_ timer: Timer) {
        UIView.animate(withDuration: 0.4, animations: { 
            self.instructions.alpha = 0.0
            }, completion: {
                completed in
                self.instructions.isHidden = true
        })
    }
}
